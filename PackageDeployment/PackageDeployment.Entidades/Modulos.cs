﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Entidades
{
    public class Modulos
    {
        private static List<string> _mods;

        public List<string> selectedModules
        {
            get
            {
                if (_mods == null)
                    _mods = new List<string>();
                return _mods;
            }
            set { _mods = value; }
        }

        public string ambienteOrig { get; set; }
    }
}
