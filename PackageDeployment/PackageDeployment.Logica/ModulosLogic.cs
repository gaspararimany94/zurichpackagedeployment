﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PackageDeployment.AccesoADatos;
using System.Configuration;
using PackageDeployment.Entidades;

namespace PackageDeployment.Logica
{
    public class ModulosLogic
    {
        public ModulosLogic()
        {
            modulos = new Modulos();
        }

        public Modulos modulos { get; set; }

        public List<string> ObtenerModulos()
        {
            Directorio direc = new Directorio();
            List<string> subFolders = direc.ObtenerModulos(modulos.ambienteOrig);
            List<string> subFolderNames = new List<string>();
            string inicioRuta =  ConfigurationManager.AppSettings["rutaSource"] + @"\" + modulos.ambienteOrig;
            int longCad = inicioRuta.Length + 1; //AGREGO 1 POR LA BARRA AL FINAL DE LA RUTA
            foreach (string folder in subFolders)
            {
                string names = folder.Substring(longCad);
                subFolderNames.Add(names);
            }
            subFolderNames.RemoveAll(s => s.StartsWith("bk")); //SE ELIMINAN LAS CARPETAS DE BACKUP
            subFolderNames.RemoveAll(s => s.StartsWith("Neoris")); //SE ELIMINAN LAS CARPETAS DEL FWK DE NEORIS
            return subFolderNames;
        }

        public void SetSelectedModules(List<string> checkedModules)
        {
            modulos.selectedModules = checkedModules;
        }

        public void SetAmbiente(string ambiente)
        {
            modulos.ambienteOrig = ambiente;
        }

        public string GetAmbiente()
        {
            return modulos.ambienteOrig;
        }

        public void ClearModulos()
        {
            modulos.selectedModules.Clear();
        }

    }

}
