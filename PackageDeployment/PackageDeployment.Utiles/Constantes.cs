﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Utiles
{
    static public class Constantes
    {
        public const string TICKET = "TKT";
        public const string INICIATIVA = "INI";

        //Pasos Instructivo Excel

        //AppPools
        public const string Paso_BajarAppPools = "Bajar appPools del servidor {0}";
        public const string Paso_LevantarAppPools = "Levantar appPools del servidor {0}";

        //Avisar a quien corresponda
        public const string Paso_Avisar_Quien_Corresponda = "Avisar a quien corresponda para coordinar las pruebas la aplicación";

        //Backups
        public const string Paso_Backup_Tablas = "Realizar Backup de las siguientes tablas: {0} del servidor {1}";
        public const string Paso_Backup_SP = "Realizar Backup de los siguientes stored procedures: {0} del servidor {1}";
        public const string Paso_Backup_Func = "Realizar Backup de las siguientes funciones: {0} del servidor {1}";
        public const string Paso_Backup_DB = "Realizar Backup de la base de datos {0}\\{1}";
        public const string Paso_Backup_Binarios = "Realizar backup de la carpeta C:\\Zurich\\{0}\\{1} del servidor {2}";
        public const string Paso_Backup_Binarios_PREPROD = "Realizar backup de la carpeta C:\\Zurich de los servidores {0}";

        //Ejecutar Scripts SQL
        public const string Paso_Ejecutar_Scripts = "Ejecutar los scripts de base datos ubicado en la carpeta DB\\{0} del paquete {1} sobre la base de datos {2}\\{0}";

        //Copiar binarios
        public const string Paso_Copiar_Binarios = "Copiar el contenido de la carpeta {0} del paquete {1} a \\\\{2}\\c$\\zurich\\{3}";

        //Borrar carpetas en Temporary ASP.NET Files
        public const string Paso_Borrar_Carpetas_Temporary_ASPNET = "Borrar las carpetas que comiencen con {0} de la ubicacion C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\Temporary ASP.NET Files";

        //Borrar StateFiles
        public const string Paso_Borrar_StateFiles = "Borrar los Archivos ubicados dentro de las carpetas StateFiles de \\\\{0}\\c$\\zurich\\{1}";
    }

    public class Servidores
    {
        public class Preprod
        {
            public const string WEB = "ARZNT0155";
            public const string HOST = "ARZNT0154";
            public const string SERVIDORDB = "ARZNT01216";
            public const string BASE = "AEC_PreProduccion";
        }

        public class DesaTest
        {
            public const string WEBHOST = "ARZNT01112";
            public const string SERVIDORDB = "ARZNT01216";
            public const string BASE_DESA = "AEC_Desa_Mejoras";
            public const string BASE_TEST = "AEC_TEST";
        }
    }

    public class ConstantesExcel
    {
        public class Hojas
        {
            public const string FORMULARIO = "Formulario";
            public const string AEC = "AEC";
        }
        
        public class Celdas
        {
            public const string NROTICKET = "C5";
            public const string NROINICIATIVA = "C6";
            public const string FECHASOLICITUD = "G5";
            public const string FECHAIMPLEMENTACION = "G6";
            public const string SOLICITANTE = "G8";
            public const string DESCRIPCION = "C11";

            public const string RESPONSABLE = "C10";
            public const string AMBIENTE = "C13";
            public const string SERVIDOR1 = "C14";
            public const string SERVIDOR2 = "D14";
            public const string SERVIDOR3 = "E14";
            public const string SERVIDOR4 = "F14";
            public const string SERVIDORDB = "D15";
            public const string BASE = "F15";
            public const string PAQUETE = "C16";
            public const string UBICACION = "E18";

            public const string ORDENIMPLEMENTACION = "B25";
            public const string TAREASIMPLEMENTACION = "C25";
        }
    }

    public class CarpetasDefault
    {
        public const string CARPETAPREPRODLOCAL = "PREPROD";
        public const string CARPETATESTLOCAL = "AEC_TESTING";
        public const string CARPETADESALOCAL = "DESA_MEJORAS";
        public const string CARPETAPRODUCCIONLOCAL = "PRODUCCION";
    }

    public class ExtensionesABorrar
    {
        public static List<string> ListaExtensiones = new List<string>(){
        "*.config",
        "*.asax",
        "*.xml",
        };
    }

     public class AmbienteDestino
    {
        public AmbienteDestino(string destino, string origen)
        {
            this.AMBIENTEDESTINO = destino;
            this.AMBIENTEORIGEN = origen;
        }
        public string AMBIENTEDESTINO;
        public string AMBIENTEORIGEN;
    }

    public class Ambientes
    {
        public static List<AmbienteDestino> ambientesList = new List<AmbienteDestino>(){
        new AmbienteDestino(CarpetasDefault.CARPETAPREPRODLOCAL, "Release"),
        new AmbienteDestino(CarpetasDefault.CARPETADESALOCAL, "MejorasR1"),
        new AmbienteDestino(CarpetasDefault.CARPETATESTLOCAL, "Main")
        };
    }

    public class Modulos
    {
         public const string propertyHost = "Zurich.Modulos.Property.Host";
         public const string propertyWeb = "Zurich.Modulos.Property.Presentacion.Web";
         public const string propertyMVC = "Zurich.Modulos.Property.Presentacion.Web.MVC";

         public const string comunHost = "Zurich.Modulos.Comun.Host";
         public const string comunWeb = "Zurich.Modulos.Comun.Presentacion.Web";

         public const string lifeHost = "Zurich.Modulos.Life.Host";
         public const string lifeWeb = "Zurich.Modulos.Life.Presentacion.Web";
         public const string lifeMVC = "Zurich.Modulos.Life.Web.Presentacion.MVC";

         public const string agentesWeb = "Agentes";
         public const string agentesHost = "Zurich.Modulos.Agentes.Host";

         public const string AECARHost = "Zurich.AEC.AR.Services.Host.IIS";
         public const string AECARWeb = "Zurich.AEC.AR.Web.MVC";

         public const string FWKHost = "Neoris.Fwk.SEC.Services.Host.IIS";
         public const string FWKHost2 = "Neoris.Fwk.Services.Host.IIS";
         public const string FWKWeb = "Zurich.Fwk.Presentacion.Web";
    }
}
