﻿using PackageDeployment.Utiles;
using System.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;

namespace PackageDeployment.Logica
{
    public class PaqueteLogic
    {
        DescripcionPaqueteLogic descPaqueteLogic;
        ModulosLogic modulosLogic;

        public PaqueteLogic(DescripcionPaqueteLogic descPaqueteLogic, ModulosLogic modulosLogic)
        {
            this.descPaqueteLogic = descPaqueteLogic;
            this.modulosLogic = modulosLogic;
        }

        public string CrearNombrePaquete()
        {
            List<string> parts = new List<string>();            
            parts.Add(GetCurrentAmbienteDest(modulosLogic.GetAmbiente()).ToUpper());
            parts.Add(NroPaquete.ResourceManager.GetString(GetCurrentAmbienteDest(modulosLogic.GetAmbiente()).ToUpper()));
            parts.Add((DateTime.Today).ToString("yyyyMMdd"));

            #region Property
            if (Contiene("Property"))
            {
                parts.Add("PTY");
                if (ContieneSubMod(Modulos.propertyHost)) parts.Add("HOST");
                if (ContieneSubMod(Modulos.propertyWeb)) parts.Add("WEB");
                if (ContieneSubMod(Modulos.propertyMVC)) parts.Add("MVC");
            }
            #endregion

            #region Comun
            if (Contiene("Comun"))
            {
                parts.Add("COMUN");
                if (ContieneSubMod(Modulos.comunHost)) parts.Add("HOST");
                if (ContieneSubMod(Modulos.comunWeb)) parts.Add("WEB");
            }
            #endregion

            #region Life
            if (Contiene("Life"))
            {
                parts.Add("LIFE");
                if (ContieneSubMod(Modulos.lifeHost)) parts.Add("HOST");
                if (ContieneSubMod(Modulos.lifeWeb)) parts.Add("WEB");
                if (ContieneSubMod(Modulos.lifeMVC)) parts.Add("MVC");
            }
            #endregion

            #region Agentes
            if (Contiene("Agentes"))
            {
                parts.Add("AGENTES");
                if (ContieneSubMod(Modulos.agentesHost)) parts.Add("HOST");
                if (ContieneSubMod(Modulos.agentesWeb)) parts.Add("WEB");
            }
            #endregion

            #region AEC.AR
            if (Contiene("AEC.AR"))
            {
                parts.Add("REG");
                if (ContieneSubMod(Modulos.AECARHost)) parts.Add("HOST");
                if (ContieneSubMod(Modulos.AECARWeb)) parts.Add("WEB");
            }
            #endregion

            return String.Join("_", parts);
        }

        private bool Contiene(string mod)
        {
            if (modulosLogic.modulos.selectedModules.Any(m => m.Contains(mod))) return true;
            else return false;
        }

        private bool ContieneSubMod(string mod)
        {
            if (modulosLogic.modulos.selectedModules.Any(m => m == mod)) return true;
            else return false;
        }

        public string GetCurrentAmbienteOrig(string dest)
        {
            return Ambientes.ambientesList.Where(s => s.AMBIENTEDESTINO == dest)
                                                    .Select(i => i.AMBIENTEORIGEN).FirstOrDefault().ToString();
        }

        public string GetCurrentAmbienteDest(string orig)
        {
            return Ambientes.ambientesList.Where(s => s.AMBIENTEORIGEN == orig)
                                                    .Select(i => i.AMBIENTEDESTINO).FirstOrDefault().ToString();
        }
    }
}
