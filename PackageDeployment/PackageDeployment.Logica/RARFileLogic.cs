﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;
using System.Configuration;
using PackageDeployment.AccesoADatos;
using PackageDeployment.Utiles;

namespace PackageDeployment.Logica
{
    public class RARFileLogic
    {
        public void ComprimirFiles(string nameFile, string ambienteSource)
        {
            try
            {
                Directorio dir = new Directorio();
                string directorioDestino = ConfigurationManager.AppSettings["paquetesLocal"];
                string directorioOrigen = ConfigurationManager.AppSettings["rutaSource"];
                string archivoDestino = directorioDestino + nameFile + ".rar";
                bool existeCarpeta = dir.chequearAcceso(directorioOrigen);
                if (existeCarpeta)
                {
                    ZipFile.CreateFromDirectory(directorioOrigen + @"\" + ambienteSource, archivoDestino);
                    ZipFile.ExtractToDirectory(archivoDestino, directorioDestino + nameFile);
                    this.BorrarArchivos(directorioDestino + nameFile);
                    this.Guardar(archivoDestino, directorioDestino + nameFile);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void BorrarArchivos(string rutaDestino)
        {
            foreach(string extension in ExtensionesABorrar.ListaExtensiones)
	        {
                string[] t = Directory.GetFiles(rutaDestino, extension, SearchOption.AllDirectories);
                Array.ForEach(t, File.Delete);
	        }            
        }

        private void Guardar(string archivoDestino, string directorioDestino)
        {
            File.Delete(archivoDestino);
            ZipFile.CreateFromDirectory(directorioDestino, archivoDestino);
            Directory.Delete(directorioDestino, true);
        }
        
    }
}
