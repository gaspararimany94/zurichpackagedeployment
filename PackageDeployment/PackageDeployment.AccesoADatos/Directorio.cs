﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;
using System.IO;
using System.Configuration;

namespace PackageDeployment.AccesoADatos
{
    public class Directorio
    {
        public bool chequearAcceso(string originPath)
        {
            try
            {
                int longitud = originPath.Length;
                if (originPath[longitud - 1].ToString() == "\\") return false;
                else if (!Directory.Exists(originPath)) return false;
                return true;
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public static string RarFiles(string rarPackagePath,
            Dictionary<int, string> accFiles, string winRARPath)
        {
            string error = "";
            try
            {
                string[] files = new string[accFiles.Count];
                int i = 0;
                foreach (var fList_item in accFiles)
                {
                    files[i] = "\"" + fList_item.Value;
                    i++;
                }
                string fileList = string.Join("\" ", files);
                fileList += "\"";
                System.Diagnostics.ProcessStartInfo sdp = new System.Diagnostics.ProcessStartInfo();
                string cmdArgs = string.Format("A {0} {1} -ep1 -r",
                    String.Format("\"{0}\"", rarPackagePath),
                    fileList);
                sdp.ErrorDialog = false;
                sdp.UseShellExecute = true;
                sdp.Arguments = cmdArgs;
                sdp.FileName = winRARPath;//Winrar.exe path
                sdp.CreateNoWindow = false;
                sdp.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                System.Diagnostics.Process process = System.Diagnostics.Process.Start(sdp);
                process.WaitForExit();
                error = "OK";
            }
            catch (Exception ex)
            {
                error = ex.Message;
            }
            return error;
        }

        public List<string> ObtenerModulos(string ambiente)
        {
            string source = ConfigurationManager.AppSettings["rutaSource"] + @"\" + ambiente; //Ambiente
            List<string> carpetas = Directory.GetDirectories(source).ToList();
            return carpetas;
        }

    }
}
