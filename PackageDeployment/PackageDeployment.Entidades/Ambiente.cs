﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Entidades
{
    public class Ambiente
    {
        public string NombreAmbiente { get; set; }
        public int UltimoNroPaquete { get; set; }
    }
}
