﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PackageDeployment.Entidades;
using System.Configuration;
using NPOI.HSSF.UserModel;
using System.IO;
using NPOI.SS.UserModel;
using PackageDeployment.Utiles;
using NPOI.XSSF.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Util;

namespace PackageDeployment.AccesoADatos
{
    public class CompletarExcel
    {
        public void CompletarDatosExcel(DatosExcel datos, string ruta)
        {
            HSSFWorkbook hssfwb;
            using (FileStream file = new FileStream(ruta, FileMode.Open, FileAccess.Read))
            {
                hssfwb = new HSSFWorkbook(file);
                file.Close();
            }

            CompletarHojaFormulario(hssfwb, datos);
            CompletarHojaAEC(hssfwb, datos);

            using (FileStream file = new FileStream(ruta, FileMode.Open, FileAccess.Write))
            {
                hssfwb.Write(file);
                file.Close();
            }
        }

        CellReference cr;

        private void CompletarHojaFormulario(HSSFWorkbook hssfwb, DatosExcel datos)
        {
            var sheet = hssfwb.GetSheet(ConstantesExcel.Hojas.FORMULARIO);

            completarDatosComunes(sheet, datos);

            cr = new CellReference(ConstantesExcel.Celdas.DESCRIPCION);
            sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(String.Join("\n", datos.descripcionCambio));
        }

        private void CompletarHojaAEC(HSSFWorkbook hssfwb, DatosExcel datos)
        {
            var sheet = hssfwb.GetSheet(ConstantesExcel.Hojas.AEC);

            completarDatosComunes(sheet, datos);

            cr = new CellReference(ConstantesExcel.Celdas.RESPONSABLE);
            sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos.desarrollador.Replace(",", "") + " / " + ObtenerResponsable());

            cr = new CellReference(ConstantesExcel.Celdas.AMBIENTE);
            sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos.ambienteDestino);

            #region completarAmbiente //TODO: Optimizar esto

            switch (datos.ambienteDestino)
            {
                case "PREPROD":
                    cr = new CellReference(ConstantesExcel.Celdas.SERVIDOR1);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.Preprod.WEB);

                    cr = new CellReference(ConstantesExcel.Celdas.SERVIDOR2);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.Preprod.HOST);

                    cr = new CellReference(ConstantesExcel.Celdas.SERVIDORDB);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.Preprod.SERVIDORDB);

                    cr = new CellReference(ConstantesExcel.Celdas.BASE);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.Preprod.BASE);
                    break;
                case "DESA_MEJORAS":
                    cr = new CellReference(ConstantesExcel.Celdas.SERVIDOR1);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.DesaTest.WEBHOST);
                    
                    cr = new CellReference(ConstantesExcel.Celdas.SERVIDORDB);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.DesaTest.SERVIDORDB);

                    cr = new CellReference(ConstantesExcel.Celdas.BASE);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.DesaTest.BASE_DESA);
                    break;
                case "AEC_TESTING":
                    cr = new CellReference(ConstantesExcel.Celdas.SERVIDOR1);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.DesaTest.WEBHOST);
                    
                    cr = new CellReference(ConstantesExcel.Celdas.SERVIDORDB);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.DesaTest.SERVIDORDB);

                    cr = new CellReference(ConstantesExcel.Celdas.BASE);
                    sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(Servidores.DesaTest.BASE_TEST);
                    break;
            }

            #endregion

            //TODO:
            cr = new CellReference(ConstantesExcel.Celdas.PAQUETE);
            //sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos);

            //TODO: Hacerlo para todos los pasos de la implementacion

            CompletarPasosImplementacion(datos, sheet, datos.ambienteDestino, hssfwb);
        }

        private void CompletarPasosImplementacion(DatosExcel datos, ISheet sheet, String ambiente, HSSFWorkbook hssfwb)
        {
            IFont font = hssfwb.CreateFont();
            font.Color = HSSFColor.Red.Index;
            font.Boldweight = (short)FontBoldWeight.Bold;

            int row = 0;

            if (datos.listaModulos.Any(m => m.Contains("Web") || m.Contains("Host")))
            {
                string paso;
                List<String> servidores = new List<string>();

                if (ambiente == "PREPROD")
                {
                    servidores.Add(Servidores.Preprod.WEB);
                    servidores.Add(Servidores.Preprod.HOST);

                    cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                    if (datos.listaModulos.Any(m => m.Contains("Web")) && datos.listaModulos.Any(m => m.Contains("Host")))
                    {
                        paso = String.Format(Constantes.Paso_BajarAppPools, servidores[0] + " y " + servidores[1]);
                        cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);

                        row += 1;

                        sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;

                        paso = String.Format(Constantes.Paso_Backup_Binarios_PREPROD, servidores[0] + " - " + servidores[1]);

                        cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                        cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).RichStringCellValue.ApplyFont(font);
                    }
                    else if (datos.listaModulos.Any(m => m.Contains("Web")))
                    {
                        paso = String.Format(Constantes.Paso_BajarAppPools, servidores[0]);
                        cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);

                        row += 1;

                        sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;

                        paso = String.Format(Constantes.Paso_Backup_Binarios_PREPROD, servidores[0]);

                        cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                        cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).RichStringCellValue.ApplyFont(font);
                    }
                    else if (datos.listaModulos.Any(m => m.Contains("Host")))
                    {
                        paso = String.Format(Constantes.Paso_BajarAppPools, servidores[1]);
                        cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);

                        row += 1;

                        sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;

                        paso = String.Format(Constantes.Paso_Backup_Binarios_PREPROD, servidores[1]);

                        cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                        cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).RichStringCellValue.ApplyFont(font);
                    }
                    

                    row += 1;

                    sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;
                }
                else
                {
                    servidores.Add(Servidores.DesaTest.WEBHOST);
                }

                if (datos.listaModulos.Any(m => m.Contains("Web")))
                {
                    paso = String.Format(Constantes.Paso_Copiar_Binarios, servidores[0], "NombrePaquete", servidores[0], "");

                    cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                    cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).RichStringCellValue.ApplyFont(font);

                    row += 1;

                    sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;

                    if (ambiente == "PREPROD")
                    {
                        paso = String.Format(Constantes.Paso_Copiar_Binarios, servidores[0], "NombrePaquete", servidores[0], "ZIMPLE");

                        cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                        cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);
                        sheet.GetRow(cr.Row + row).GetCell(cr.Col).RichStringCellValue.ApplyFont(font);

                        row += 1;

                        sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;
                    }
                }

                if (datos.listaModulos.Any(m => m.Contains("Host")))
                {
                    if (ambiente == "PREPROD")
                    {
                        paso = String.Format(Constantes.Paso_Copiar_Binarios, servidores[1], "NombrePaquete", servidores[1], "");
                    }
                    else
                    {
                        paso = String.Format(Constantes.Paso_Copiar_Binarios, servidores[0], "NombrePaquete", servidores[0], "");
                    }
                    
                    cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                    cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).RichStringCellValue.ApplyFont(font);

                    row += 1;

                    sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;
                }

                cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                if (datos.listaModulos.Any(m => m.Contains("Web")) && datos.listaModulos.Any(m => m.Contains("Host")))
                {
                    paso = String.Format(Constantes.Paso_LevantarAppPools, servidores[0] + " y " + servidores[1]);
                    cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);

                    row += 1;

                    sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;
                }
                else if (datos.listaModulos.Any(m => m.Contains("Web")))
                {
                    paso = String.Format(Constantes.Paso_BajarAppPools, servidores[0]);
                    cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);

                    row += 1;

                    sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;
                }
                else if (datos.listaModulos.Any(m => m.Contains("Host")))
                {
                    paso = String.Format(Constantes.Paso_BajarAppPools, servidores[1]);
                    cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                    sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);

                    row += 1;

                    sheet.CopyRow(cr.Row, cr.Row + row).Height = sheet.GetRow(cr.Row).Height;
                }

                paso = String.Format(Constantes.Paso_Avisar_Quien_Corresponda);

                cr = new CellReference(ConstantesExcel.Celdas.ORDENIMPLEMENTACION);
                sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(row + 1);

                cr = new CellReference(ConstantesExcel.Celdas.TAREASIMPLEMENTACION);
                sheet.GetRow(cr.Row + row).GetCell(cr.Col).SetCellValue(paso);
            }
        }

        private void completarDatosComunes(ISheet sheet, DatosExcel datos)
        {
            if (datos.tipo == Constantes.TICKET)
            {
                cr = new CellReference(ConstantesExcel.Celdas.NROTICKET);
                sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos.nro);
            }
            else
            {
                cr = new CellReference(ConstantesExcel.Celdas.NROINICIATIVA);
                sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos.nro);
            }

            cr = new CellReference(ConstantesExcel.Celdas.FECHASOLICITUD);
            sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos.fechaSolicitud);

            cr = new CellReference(ConstantesExcel.Celdas.FECHAIMPLEMENTACION);
            sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos.fechaImplementacion);

            cr = new CellReference(ConstantesExcel.Celdas.SOLICITANTE);
            sheet.GetRow(cr.Row).GetCell(cr.Col).SetCellValue(datos.solicitante);
        }

        public static string ObtenerResponsable()
        {
            string userName = Environment.UserName;
            userName = userName.Replace(".", " ");
            userName = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(userName.ToLower());
            return userName;
        }
    }
}
