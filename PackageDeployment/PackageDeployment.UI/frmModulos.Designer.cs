﻿namespace PackageDeployment.UI
{
    partial class frmModulos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listChkModulos = new System.Windows.Forms.CheckedListBox();
            this.gbModulosList = new System.Windows.Forms.GroupBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.gbModulosList.SuspendLayout();
            this.SuspendLayout();
            // 
            // listChkModulos
            // 
            this.listChkModulos.BackColor = System.Drawing.Color.LightGray;
            this.listChkModulos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listChkModulos.CheckOnClick = true;
            this.listChkModulos.FormattingEnabled = true;
            this.listChkModulos.Location = new System.Drawing.Point(6, 19);
            this.listChkModulos.Name = "listChkModulos";
            this.listChkModulos.Size = new System.Drawing.Size(300, 227);
            this.listChkModulos.TabIndex = 0;
            // 
            // gbModulosList
            // 
            this.gbModulosList.BackColor = System.Drawing.Color.White;
            this.gbModulosList.Controls.Add(this.btnAceptar);
            this.gbModulosList.Controls.Add(this.listChkModulos);
            this.gbModulosList.Location = new System.Drawing.Point(12, 12);
            this.gbModulosList.Name = "gbModulosList";
            this.gbModulosList.Size = new System.Drawing.Size(306, 283);
            this.gbModulosList.TabIndex = 1;
            this.gbModulosList.TabStop = false;
            this.gbModulosList.Text = "Modulos";
            // 
            // btnAceptar
            // 
            this.btnAceptar.BackColor = System.Drawing.Color.LightGray;
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAceptar.Location = new System.Drawing.Point(225, 254);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 1;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // frmModulos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(330, 304);
            this.Controls.Add(this.gbModulosList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmModulos";
            this.Text = "Modulos";
            this.gbModulosList.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox listChkModulos;
        private System.Windows.Forms.GroupBox gbModulosList;
        private System.Windows.Forms.Button btnAceptar;
    }
}