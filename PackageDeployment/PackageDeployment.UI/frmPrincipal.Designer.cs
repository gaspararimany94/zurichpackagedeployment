﻿namespace PackageDeployment.UI
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ToolStrip toolStrip1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.btnNuevoPaquete = new System.Windows.Forms.ToolStripButton();
            this.btnPlanillaProd = new System.Windows.Forms.ToolStripButton();
            this.btnConfig = new System.Windows.Forms.ToolStripButton();
            this.btnInstruc = new System.Windows.Forms.ToolStripButton();
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.gbModulos = new System.Windows.Forms.GroupBox();
            this.lblOrigen = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbSelectedModules = new System.Windows.Forms.ListBox();
            this.btnSeleccionarModulos = new System.Windows.Forms.Button();
            this.lblModulos = new System.Windows.Forms.Label();
            this.lblDestino = new System.Windows.Forms.Label();
            this.cbDestino = new System.Windows.Forms.ComboBox();
            this.gbEnvia = new System.Windows.Forms.GroupBox();
            this.tbMaskNumero = new System.Windows.Forms.MaskedTextBox();
            this.lblNumero = new System.Windows.Forms.Label();
            this.rbIniciativa = new System.Windows.Forms.RadioButton();
            this.rbTicket = new System.Windows.Forms.RadioButton();
            this.gbDesc = new System.Windows.Forms.GroupBox();
            this.btnDescripcion = new System.Windows.Forms.Button();
            this.lblConsideraciones = new System.Windows.Forms.Label();
            this.lblCP = new System.Windows.Forms.Label();
            this.lblDesc = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDesarrolladorNombre = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSolicitanteNombre = new System.Windows.Forms.Label();
            this.lblSolicitante = new System.Windows.Forms.Label();
            this.lblNro = new System.Windows.Forms.Label();
            this.lblImplementacion = new System.Windows.Forms.Label();
            this.toolStripContainer2 = new System.Windows.Forms.ToolStripContainer();
            this.btnComprimir = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            toolStrip1 = new System.Windows.Forms.ToolStrip();
            toolStrip1.SuspendLayout();
            this.gbModulos.SuspendLayout();
            this.gbEnvia.SuspendLayout();
            this.gbDesc.SuspendLayout();
            this.toolStripContainer2.ContentPanel.SuspendLayout();
            this.toolStripContainer2.LeftToolStripPanel.SuspendLayout();
            this.toolStripContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            toolStrip1.BackColor = System.Drawing.Color.Orange;
            toolStrip1.CanOverflow = false;
            toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            toolStrip1.ImageScalingSize = new System.Drawing.Size(46, 46);
            toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNuevoPaquete,
            this.btnPlanillaProd,
            this.btnConfig,
            this.btnInstruc});
            toolStrip1.Location = new System.Drawing.Point(0, 0);
            toolStrip1.Name = "toolStrip1";
            toolStrip1.ShowItemToolTips = false;
            toolStrip1.Size = new System.Drawing.Size(51, 419);
            toolStrip1.Stretch = true;
            toolStrip1.TabIndex = 0;
            // 
            // btnNuevoPaquete
            // 
            this.btnNuevoPaquete.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNuevoPaquete.Image = ((System.Drawing.Image)(resources.GetObject("btnNuevoPaquete.Image")));
            this.btnNuevoPaquete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNuevoPaquete.Name = "btnNuevoPaquete";
            this.btnNuevoPaquete.Size = new System.Drawing.Size(49, 50);
            this.btnNuevoPaquete.Text = "Nuevo Paquete";
            // 
            // btnPlanillaProd
            // 
            this.btnPlanillaProd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPlanillaProd.Image = ((System.Drawing.Image)(resources.GetObject("btnPlanillaProd.Image")));
            this.btnPlanillaProd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPlanillaProd.Name = "btnPlanillaProd";
            this.btnPlanillaProd.Size = new System.Drawing.Size(49, 50);
            this.btnPlanillaProd.Text = "Planilla Produccion";
            // 
            // btnConfig
            // 
            this.btnConfig.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnConfig.Image = ((System.Drawing.Image)(resources.GetObject("btnConfig.Image")));
            this.btnConfig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(49, 50);
            this.btnConfig.Text = "Configuraciones";
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // btnInstruc
            // 
            this.btnInstruc.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnInstruc.Image = ((System.Drawing.Image)(resources.GetObject("btnInstruc.Image")));
            this.btnInstruc.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnInstruc.Name = "btnInstruc";
            this.btnInstruc.Size = new System.Drawing.Size(49, 50);
            this.btnInstruc.Text = "Instrucciones";
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.Size = new System.Drawing.Size(150, 150);
            // 
            // gbModulos
            // 
            this.gbModulos.BackColor = System.Drawing.Color.Transparent;
            this.gbModulos.Controls.Add(this.lblOrigen);
            this.gbModulos.Controls.Add(this.label1);
            this.gbModulos.Controls.Add(this.lbSelectedModules);
            this.gbModulos.Controls.Add(this.btnSeleccionarModulos);
            this.gbModulos.Controls.Add(this.lblModulos);
            this.gbModulos.Controls.Add(this.lblDestino);
            this.gbModulos.Controls.Add(this.cbDestino);
            this.gbModulos.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbModulos.Location = new System.Drawing.Point(3, 3);
            this.gbModulos.Name = "gbModulos";
            this.gbModulos.Size = new System.Drawing.Size(254, 355);
            this.gbModulos.TabIndex = 0;
            this.gbModulos.TabStop = false;
            this.gbModulos.Text = "Ambiente y Modulos";
            // 
            // lblOrigen
            // 
            this.lblOrigen.AutoSize = true;
            this.lblOrigen.Location = new System.Drawing.Point(69, 47);
            this.lblOrigen.Name = "lblOrigen";
            this.lblOrigen.Size = new System.Drawing.Size(0, 13);
            this.lblOrigen.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Origen:";
            // 
            // lbSelectedModules
            // 
            this.lbSelectedModules.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lbSelectedModules.FormattingEnabled = true;
            this.lbSelectedModules.Location = new System.Drawing.Point(6, 89);
            this.lbSelectedModules.Name = "lbSelectedModules";
            this.lbSelectedModules.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lbSelectedModules.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbSelectedModules.Size = new System.Drawing.Size(242, 225);
            this.lbSelectedModules.TabIndex = 0;
            // 
            // btnSeleccionarModulos
            // 
            this.btnSeleccionarModulos.BackColor = System.Drawing.Color.LightGray;
            this.btnSeleccionarModulos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeleccionarModulos.Location = new System.Drawing.Point(157, 326);
            this.btnSeleccionarModulos.Name = "btnSeleccionarModulos";
            this.btnSeleccionarModulos.Size = new System.Drawing.Size(75, 23);
            this.btnSeleccionarModulos.TabIndex = 3;
            this.btnSeleccionarModulos.Text = "Seleccionar";
            this.btnSeleccionarModulos.UseVisualStyleBackColor = false;
            this.btnSeleccionarModulos.Click += new System.EventHandler(this.btnSeleccionarModulos_Click);
            // 
            // lblModulos
            // 
            this.lblModulos.AutoSize = true;
            this.lblModulos.Location = new System.Drawing.Point(6, 73);
            this.lblModulos.Name = "lblModulos";
            this.lblModulos.Size = new System.Drawing.Size(124, 13);
            this.lblModulos.TabIndex = 0;
            this.lblModulos.Text = "Modulos seleccionados: ";
            // 
            // lblDestino
            // 
            this.lblDestino.AutoSize = true;
            this.lblDestino.Location = new System.Drawing.Point(21, 22);
            this.lblDestino.Name = "lblDestino";
            this.lblDestino.Size = new System.Drawing.Size(46, 13);
            this.lblDestino.TabIndex = 1;
            this.lblDestino.Text = "Destino:";
            // 
            // cbDestino
            // 
            this.cbDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDestino.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbDestino.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.cbDestino.FormattingEnabled = true;
            this.cbDestino.Location = new System.Drawing.Point(73, 19);
            this.cbDestino.Name = "cbDestino";
            this.cbDestino.Size = new System.Drawing.Size(121, 21);
            this.cbDestino.TabIndex = 0;
            this.cbDestino.SelectedIndexChanged += new System.EventHandler(this.cbDestino_SelectedIndexChanged);
            // 
            // gbEnvia
            // 
            this.gbEnvia.Controls.Add(this.tbMaskNumero);
            this.gbEnvia.Controls.Add(this.lblNumero);
            this.gbEnvia.Controls.Add(this.rbIniciativa);
            this.gbEnvia.Controls.Add(this.rbTicket);
            this.gbEnvia.Location = new System.Drawing.Point(263, 3);
            this.gbEnvia.Name = "gbEnvia";
            this.gbEnvia.Size = new System.Drawing.Size(325, 99);
            this.gbEnvia.TabIndex = 2;
            this.gbEnvia.TabStop = false;
            this.gbEnvia.Text = "Que se envía?";
            // 
            // tbMaskNumero
            // 
            this.tbMaskNumero.Location = new System.Drawing.Point(72, 57);
            this.tbMaskNumero.Mask = "999999";
            this.tbMaskNumero.Name = "tbMaskNumero";
            this.tbMaskNumero.Size = new System.Drawing.Size(48, 20);
            this.tbMaskNumero.TabIndex = 4;
            this.tbMaskNumero.Leave += new System.EventHandler(this.tbMaskNumero_Leave);
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblNumero.Location = new System.Drawing.Point(16, 60);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(50, 13);
            this.lblNumero.TabIndex = 2;
            this.lblNumero.Text = "Numero: ";
            // 
            // rbIniciativa
            // 
            this.rbIniciativa.AutoSize = true;
            this.rbIniciativa.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rbIniciativa.Location = new System.Drawing.Point(88, 23);
            this.rbIniciativa.Name = "rbIniciativa";
            this.rbIniciativa.Size = new System.Drawing.Size(66, 17);
            this.rbIniciativa.TabIndex = 1;
            this.rbIniciativa.TabStop = true;
            this.rbIniciativa.Text = "Iniciativa";
            this.rbIniciativa.UseVisualStyleBackColor = true;
            this.rbIniciativa.CheckedChanged += new System.EventHandler(this.rbIniciativa_CheckedChanged);
            // 
            // rbTicket
            // 
            this.rbTicket.AutoSize = true;
            this.rbTicket.BackColor = System.Drawing.Color.Transparent;
            this.rbTicket.Checked = true;
            this.rbTicket.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.rbTicket.Location = new System.Drawing.Point(16, 23);
            this.rbTicket.Name = "rbTicket";
            this.rbTicket.Size = new System.Drawing.Size(54, 17);
            this.rbTicket.TabIndex = 0;
            this.rbTicket.TabStop = true;
            this.rbTicket.Text = "Ticket";
            this.rbTicket.UseVisualStyleBackColor = false;
            this.rbTicket.CheckedChanged += new System.EventHandler(this.rbTicket_CheckedChanged);
            // 
            // gbDesc
            // 
            this.gbDesc.Controls.Add(this.btnDescripcion);
            this.gbDesc.Controls.Add(this.lblConsideraciones);
            this.gbDesc.Controls.Add(this.lblCP);
            this.gbDesc.Controls.Add(this.lblDesc);
            this.gbDesc.Controls.Add(this.label2);
            this.gbDesc.Controls.Add(this.lblDesarrolladorNombre);
            this.gbDesc.Controls.Add(this.label4);
            this.gbDesc.Controls.Add(this.lblSolicitanteNombre);
            this.gbDesc.Controls.Add(this.lblSolicitante);
            this.gbDesc.Controls.Add(this.lblNro);
            this.gbDesc.Controls.Add(this.lblImplementacion);
            this.gbDesc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbDesc.Location = new System.Drawing.Point(263, 108);
            this.gbDesc.Name = "gbDesc";
            this.gbDesc.Size = new System.Drawing.Size(413, 218);
            this.gbDesc.TabIndex = 3;
            this.gbDesc.TabStop = false;
            this.gbDesc.Text = "Descripcion del cambio";
            // 
            // btnDescripcion
            // 
            this.btnDescripcion.BackColor = System.Drawing.Color.LightGray;
            this.btnDescripcion.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDescripcion.Location = new System.Drawing.Point(320, 19);
            this.btnDescripcion.Name = "btnDescripcion";
            this.btnDescripcion.Size = new System.Drawing.Size(88, 23);
            this.btnDescripcion.TabIndex = 10;
            this.btnDescripcion.Text = "Agregar/Editar";
            this.btnDescripcion.UseVisualStyleBackColor = false;
            this.btnDescripcion.Click += new System.EventHandler(this.btnDescripcion_Click);
            // 
            // lblConsideraciones
            // 
            this.lblConsideraciones.AutoSize = true;
            this.lblConsideraciones.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsideraciones.Location = new System.Drawing.Point(44, 161);
            this.lblConsideraciones.Name = "lblConsideraciones";
            this.lblConsideraciones.Size = new System.Drawing.Size(112, 14);
            this.lblConsideraciones.TabIndex = 9;
            this.lblConsideraciones.Text = "Consideraciones";
            this.lblConsideraciones.Visible = false;
            // 
            // lblCP
            // 
            this.lblCP.AutoSize = true;
            this.lblCP.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblCP.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblCP.Location = new System.Drawing.Point(6, 137);
            this.lblCP.Name = "lblCP";
            this.lblCP.Size = new System.Drawing.Size(175, 15);
            this.lblCP.TabIndex = 8;
            this.lblCP.Text = "Consideraciones previas:";
            this.lblCP.Visible = false;
            // 
            // lblDesc
            // 
            this.lblDesc.AutoSize = true;
            this.lblDesc.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.Location = new System.Drawing.Point(44, 97);
            this.lblDesc.MaximumSize = new System.Drawing.Size(400, 70);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(84, 14);
            this.lblDesc.TabIndex = 7;
            this.lblDesc.Text = "Descripcion";
            this.lblDesc.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label2.Location = new System.Drawing.Point(6, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Descripcion del cambio:";
            // 
            // lblDesarrolladorNombre
            // 
            this.lblDesarrolladorNombre.AutoSize = true;
            this.lblDesarrolladorNombre.Font = new System.Drawing.Font("Consolas", 9F);
            this.lblDesarrolladorNombre.Location = new System.Drawing.Point(118, 59);
            this.lblDesarrolladorNombre.Name = "lblDesarrolladorNombre";
            this.lblDesarrolladorNombre.Size = new System.Drawing.Size(49, 14);
            this.lblDesarrolladorNombre.TabIndex = 5;
            this.lblDesarrolladorNombre.Text = "Nombre";
            this.lblDesarrolladorNombre.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label4.Location = new System.Drawing.Point(7, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Desarrollador:";
            // 
            // lblSolicitanteNombre
            // 
            this.lblSolicitanteNombre.AutoSize = true;
            this.lblSolicitanteNombre.Font = new System.Drawing.Font("Consolas", 9F);
            this.lblSolicitanteNombre.Location = new System.Drawing.Point(103, 41);
            this.lblSolicitanteNombre.Name = "lblSolicitanteNombre";
            this.lblSolicitanteNombre.Size = new System.Drawing.Size(49, 14);
            this.lblSolicitanteNombre.TabIndex = 3;
            this.lblSolicitanteNombre.Text = "Nombre";
            this.lblSolicitanteNombre.Visible = false;
            // 
            // lblSolicitante
            // 
            this.lblSolicitante.AutoSize = true;
            this.lblSolicitante.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblSolicitante.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblSolicitante.Location = new System.Drawing.Point(6, 40);
            this.lblSolicitante.Name = "lblSolicitante";
            this.lblSolicitante.Size = new System.Drawing.Size(91, 15);
            this.lblSolicitante.TabIndex = 2;
            this.lblSolicitante.Text = "Solicitante:";
            // 
            // lblNro
            // 
            this.lblNro.AutoSize = true;
            this.lblNro.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNro.Location = new System.Drawing.Point(42, 25);
            this.lblNro.Name = "lblNro";
            this.lblNro.Size = new System.Drawing.Size(28, 14);
            this.lblNro.TabIndex = 1;
            this.lblNro.Text = "Nro";
            this.lblNro.Visible = false;
            // 
            // lblImplementacion
            // 
            this.lblImplementacion.AutoSize = true;
            this.lblImplementacion.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImplementacion.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblImplementacion.Location = new System.Drawing.Point(7, 24);
            this.lblImplementacion.Name = "lblImplementacion";
            this.lblImplementacion.Size = new System.Drawing.Size(35, 15);
            this.lblImplementacion.TabIndex = 0;
            this.lblImplementacion.Text = "TKT:";
            this.lblImplementacion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toolStripContainer2
            // 
            this.toolStripContainer2.BottomToolStripPanelVisible = false;
            // 
            // toolStripContainer2.ContentPanel
            // 
            this.toolStripContainer2.ContentPanel.BackColor = System.Drawing.Color.White;
            this.toolStripContainer2.ContentPanel.Controls.Add(this.button1);
            this.toolStripContainer2.ContentPanel.Controls.Add(this.btnComprimir);
            this.toolStripContainer2.ContentPanel.Controls.Add(this.gbDesc);
            this.toolStripContainer2.ContentPanel.Controls.Add(this.gbEnvia);
            this.toolStripContainer2.ContentPanel.Controls.Add(this.gbModulos);
            this.toolStripContainer2.ContentPanel.Size = new System.Drawing.Size(689, 419);
            this.toolStripContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // toolStripContainer2.LeftToolStripPanel
            // 
            this.toolStripContainer2.LeftToolStripPanel.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.toolStripContainer2.LeftToolStripPanel.Controls.Add(toolStrip1);
            this.toolStripContainer2.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer2.Name = "toolStripContainer2";
            this.toolStripContainer2.RightToolStripPanelVisible = false;
            this.toolStripContainer2.Size = new System.Drawing.Size(740, 419);
            this.toolStripContainer2.TabIndex = 0;
            this.toolStripContainer2.TopToolStripPanelVisible = false;
            // 
            // btnComprimir
            // 
            this.btnComprimir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnComprimir.BackgroundImage")));
            this.btnComprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnComprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnComprimir.ForeColor = System.Drawing.Color.Transparent;
            this.btnComprimir.Location = new System.Drawing.Point(615, 22);
            this.btnComprimir.Name = "btnComprimir";
            this.btnComprimir.Size = new System.Drawing.Size(56, 67);
            this.btnComprimir.TabIndex = 4;
            this.btnComprimir.UseVisualStyleBackColor = true;
            this.btnComprimir.Click += new System.EventHandler(this.btnComprimir_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(595, 328);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(740, 419);
            this.Controls.Add(this.toolStripContainer2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MaximizeBox = false;
            this.Name = "frmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AEC - Zurich Package Deployment";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPrincipal_FormClosed);
            toolStrip1.ResumeLayout(false);
            toolStrip1.PerformLayout();
            this.gbModulos.ResumeLayout(false);
            this.gbModulos.PerformLayout();
            this.gbEnvia.ResumeLayout(false);
            this.gbEnvia.PerformLayout();
            this.gbDesc.ResumeLayout(false);
            this.gbDesc.PerformLayout();
            this.toolStripContainer2.ContentPanel.ResumeLayout(false);
            this.toolStripContainer2.LeftToolStripPanel.ResumeLayout(false);
            this.toolStripContainer2.LeftToolStripPanel.PerformLayout();
            this.toolStripContainer2.ResumeLayout(false);
            this.toolStripContainer2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.GroupBox gbModulos;
        private System.Windows.Forms.Button btnSeleccionarModulos;
        private System.Windows.Forms.Label lblModulos;
        private System.Windows.Forms.Label lblDestino;
        private System.Windows.Forms.ComboBox cbDestino;
        private System.Windows.Forms.GroupBox gbEnvia;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.RadioButton rbIniciativa;
        private System.Windows.Forms.RadioButton rbTicket;
        private System.Windows.Forms.GroupBox gbDesc;
        private System.Windows.Forms.Button btnDescripcion;
        private System.Windows.Forms.Label lblConsideraciones;
        private System.Windows.Forms.Label lblCP;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDesarrolladorNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSolicitanteNombre;
        private System.Windows.Forms.Label lblSolicitante;
        private System.Windows.Forms.Label lblNro;
        private System.Windows.Forms.Label lblImplementacion;
        private System.Windows.Forms.ToolStripContainer toolStripContainer2;
        private System.Windows.Forms.ToolStripButton btnNuevoPaquete;
        private System.Windows.Forms.ToolStripButton btnPlanillaProd;
        private System.Windows.Forms.ToolStripButton btnConfig;
        private System.Windows.Forms.ToolStripButton btnInstruc;
        private System.Windows.Forms.MaskedTextBox tbMaskNumero;
        private System.Windows.Forms.Button btnComprimir;
        private System.Windows.Forms.ListBox lbSelectedModules;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblOrigen;
        private System.Windows.Forms.Button button1;

    }
}

