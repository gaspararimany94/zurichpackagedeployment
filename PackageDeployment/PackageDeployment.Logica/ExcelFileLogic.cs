﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using PackageDeployment.AccesoADatos;
using System.IO;
using PackageDeployment.Entidades;
using PackageDeployment.Utiles;
using System.Reflection;

namespace PackageDeployment.Logica
{
    public class ExcelFileLogic
    {
        string nombrePlanilla;
        string destFile;
        public void CopiarExcel(string ambiente)
        {
            string sourceFile = ConfigurationManager.AppSettings["rutaExcelOrigen"];
            destFile = string.Concat(ConfigurationManager.AppSettings["rutaPackage"], @"\", ambiente);
            destFile = destFile.Replace("{0}", DateTime.Now.Year.ToString());
            string fecha = DateTime.Now.ToShortDateString();

            int nroPaquete = ObtenerUltimoNroPaquete(ambiente);

            nombrePlanilla = "Implementacion_" + ambiente + " " + nroPaquete + " " + fecha.Replace("/", "-") + ".xls";
            
            Directorio dir = new Directorio();
            if (!dir.chequearAcceso(sourceFile))
            {
                try
                {
                    File.Copy(sourceFile, string.Concat(destFile, @"\", nombrePlanilla), true);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public int ObtenerUltimoNroPaquete(string ambiente)
        {
            Catalogo catalogo = new Catalogo();
            return catalogo.ObtenerNroPaquete(ambiente);
        }

        public void CompletarDatosExcel(DatosExcel datos)
        {
            CompletarExcel datosExcel = new CompletarExcel();
            datosExcel.CompletarDatosExcel(datos, string.Concat(destFile, @"\", nombrePlanilla));
        }
    }
}