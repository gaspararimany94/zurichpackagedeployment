﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PackageDeployment.Logica;

namespace PackageDeployment.UI
{
    public partial class frmModulos : Form
    {
        public ModulosLogic modsLogic { get; set; }

        public frmModulos(ModulosLogic modulosLogic)
        {
            InitializeComponent();
            this.modsLogic = modulosLogic;
            this.cargarListaModulos();
        }

        private void cargarListaModulos()
        {
            gbModulosList.Text = "Modulos de " + modsLogic.modulos.ambienteOrig;
            ((ListBox)listChkModulos).DataSource = modsLogic.ObtenerModulos();
            this.CheckSelectedModules();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            List<string> modulosSeleccionados = new List<string>();
            foreach (object item in listChkModulos.CheckedItems)
            {
                modulosSeleccionados.Add(item.ToString());
            }
            this.modsLogic.SetSelectedModules(modulosSeleccionados);
            this.Close();
        }

        private void CheckSelectedModules()
        {
            List<string> listaContutti = modsLogic.ObtenerModulos();
            List<string> otralista = modsLogic.modulos.selectedModules;

            CheckedListBox clb = listChkModulos;
            //listChkModulos.IndexFromPoint
            
            foreach (var i in otralista)
            {
               var index = listaContutti.IndexOf(i);
               clb.SetItemChecked(index, true);
            }
        }
    }
}
