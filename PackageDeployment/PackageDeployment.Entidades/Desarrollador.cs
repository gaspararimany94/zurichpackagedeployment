﻿using PackageDeployment.Utiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Entidades
{
    public class Desarrollador : PackageDeploymentEntity
    {
        public const String NombrePropertyName = "Nombre";
        public String Nombre { get; set; }

        public const String ApellidoPropertyName = "Apellido";
        public String Apellido { get; set; }

        public const String MailPropertyName = "Mail";
        public String Mail { get; set; }

        public String NombreCompleto { get { return Nombre + ", " + Apellido; } }

        public override void Validate()
        {
            base.Validate();

            if (string.IsNullOrEmpty(Nombre))
            {
                throw new Exception(String.Format(Mensajes.ElCampoXEsDeIngresoObligatorio, NombrePropertyName));
            }

            if (string.IsNullOrEmpty(Apellido))
            {
                throw new Exception(String.Format(Mensajes.ElCampoXEsDeIngresoObligatorio, ApellidoPropertyName));
            }

            if (string.IsNullOrEmpty(Mail))
            {
                throw new Exception(String.Format(Mensajes.ElCampoXEsDeIngresoObligatorio, MailPropertyName));
            }

        }
    }
}
