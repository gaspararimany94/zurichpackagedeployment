﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.AccesoADatos
{
    public interface IEntityToXmlServices
    {
        /// <summary>
        /// Guarda un objeto cualquiera de tipo T en el xml
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="entityType"></param>
        void SaveListOfEntities<T>(object obj);

        List<T> GetListOfEntities<T>();
    }
}
