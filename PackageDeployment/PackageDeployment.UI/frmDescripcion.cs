﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PackageDeployment.Logica;
using PackageDeployment.Entidades;
using PackageDeployment.Utiles;

namespace PackageDeployment.UI
{
    public partial class frmDescripcion : Form
    {
        public DescripcionPaqueteLogic descPaqueteLogic { get; set; }
        private readonly IEntityToXMLLogic entityToXMLLogic;
        
        public frmDescripcion(DescripcionPaqueteLogic descPaqueteLogic)
        {
            InitializeComponent();
            this.descPaqueteLogic = descPaqueteLogic;
            this.mappearDatosADetalles();
            entityToXMLLogic = new EntityToXMLLogic();
            this.CargarCombos();
        }
        
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            DescripcionPaquete descripcionPaquete = this.descPaqueteLogic.descPaquete;

            descripcionPaquete.desarrollador = cbDesarrollador.Text;
            descripcionPaquete.solicitante = cbSolicitante.Text;
            descripcionPaquete.descripcionCambio = tbDescripcionCambio.Text.Split('\n');
            descripcionPaquete.sePuedeEditar = true;
            descripcionPaquete.consideracionesPrevias = tbConsideraciones.Text.Split('\n');
           
            this.descPaqueteLogic.descPaquete = descripcionPaquete;
            this.Close();
        }

        private void mappearDatosADetalles()
        {
            if (this.descPaqueteLogic.descPaquete.sePuedeEditar)
            {
                this.tbConsideraciones.Text = String.Join("\n", this.descPaqueteLogic.descPaquete.consideracionesPrevias).Trim();
                this.tbDescripcionCambio.Text = String.Join("\n",this.descPaqueteLogic.descPaquete.descripcionCambio).Trim();
                this.cbDesarrollador.Text = this.descPaqueteLogic.descPaquete.desarrollador;
                this.cbSolicitante.Text = this.descPaqueteLogic.descPaquete.solicitante;
            }
        }

        private void CargarCombos()
        {
            cbDesarrollador.DataSource = entityToXMLLogic.GetListOfEntities<Desarrollador>();
            cbSolicitante.DataSource = entityToXMLLogic.GetListOfEntities<Solicitante>();
            cbDesarrollador.DisplayMember = "NombreCompleto";
            cbSolicitante.DisplayMember = "NombreCompleto";
        }
    }
}
