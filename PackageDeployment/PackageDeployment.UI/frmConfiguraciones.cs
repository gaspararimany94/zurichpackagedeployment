﻿using PackageDeployment.Entidades;
using PackageDeployment.Utiles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PackageDeployment.UI
{
    public partial class frmConfiguraciones : Form
    {
        #region Members

        private readonly Logica.EntityToXMLLogic configuracionesLogic;

        private List<Solicitante> ListaSolicitantes;

        private List<Desarrollador> ListaDesarrolladores;

        private Solicitante SolicitanteSeleccionado;

        private Desarrollador DesarrolladorSeleccionado;

        #endregion

        #region Constructor

        public frmConfiguraciones()
        {
            InitializeComponent();

            InicializarControles();

            this.configuracionesLogic = new Logica.EntityToXMLLogic();

            ListaSolicitantes = new List<Solicitante>();

            ListaDesarrolladores = new List<Desarrollador>();

            CargarSolicitantes();

            CargarDesarrolladores();

            ModoVentanaSolicitantes = ModosVentanaSolicitante.Consulta;

            ModoVentanaDesarrolladores = ModosVentanaDesarrollador.Consulta;
        }

        #endregion

        #region Properties

        private ModosVentanaSolicitante _ModoSolicitante;
        private ModosVentanaSolicitante ModoVentanaSolicitantes
        {
            get
            {
                return this._ModoSolicitante;
            }
            set
            {
                this._ModoSolicitante = value;

                if (value == ModosVentanaSolicitante.Consulta)
                {
                    this.groupBoxDatosSolicitante.Enabled = false;

                    this.btnNuevoSolicitante.Enabled = true;

                    this.btnGuardarSolicitante.Enabled = false;

                    this.btnEliminarSolicitante.Enabled = false;
                }

                if (value == ModosVentanaSolicitante.Alta)
                {
                    LimpiarCamposSolicitante();

                    this.groupBoxDatosSolicitante.Enabled = true;

                    this.btnEditarSolicitante.Enabled = false;

                    this.btnGuardarSolicitante.Enabled = true;
                }

                if (value == ModosVentanaSolicitante.Edicion)
                {
                    this.groupBoxDatosSolicitante.Enabled = true;

                    this.btnGuardarSolicitante.Enabled = true;

                    MostrarDatosSolicitanteSeleccionado();
                }
            }
        }

        private enum ModosVentanaSolicitante
        {
            Alta,
            Edicion,
            Consulta
        }

        public bool HayItemsSeleccionadosEnListaSolicitantes { get { return listBoxSolicitantes.SelectedIndex != -1 ? true : false; } }

        #endregion        

        #region Methods

        private void InicializarControles()
        {
            this.groupBoxDatosSolicitante.Enabled = false;

            this.tbcConfiguraciones.TabPages[0].Text = Etiquetas.Solicitantes;

            this.groupListaSolicitantes.Text = Etiquetas.ListaSolicitantes;
            
            this.groupBoxDatosSolicitante.Text = Etiquetas.DatosSolicitante;

            this.tbcConfiguraciones.TabPages[1].Text = Etiquetas.Desarrolladores;

            this.groupBoxListaDesarrolladores.Text = Etiquetas.ListaDesarrolladores;

            this.groupBoxDatosDesarrollador.Text = Etiquetas.DatosDesarrollador;


            #region Buttons

            this.btnNuevoSolicitante.Enabled = true;
            this.btnEditarSolicitante.Enabled = false;
            this.btnGuardarSolicitante.Enabled = false;
            this.btnSalirSolicitante.Enabled = true;

            this.btnNuevoDesarrollador.Enabled = true;
            this.btnEditarDesarrollador.Enabled = false;
            this.btnGuardarDesarrollador.Enabled = false;
            this.btnSalirDesarrollador.Enabled = true;

            #endregion
        }

        private void CargarSolicitantes()
        {
            CargarListaSolicitantes();

            listBoxSolicitantes.DataSource = ListaSolicitantes;

            listBoxSolicitantes.DisplayMember = "NombreCompleto";
            listBoxSolicitantes.ClearSelected();

            this.btnEditarSolicitante.Enabled = false;

            LimpiarCamposSolicitante();
        }

        private void LimpiarCamposSolicitante()
        {
            this.txtNombreSolicitante.Text = String.Empty;
            this.txtApellidoSolicitante.Text = String.Empty;
            this.txtMailSolicitante.Text = String.Empty;
        }

        private void CargarListaSolicitantes()
        {
            ListaSolicitantes = this.configuracionesLogic.GetListOfEntities<Solicitante>();
        }

        private void MostrarDatosSolicitanteSeleccionado()
        {
            this.txtNombreSolicitante.Text = SolicitanteSeleccionado.Nombre;
            this.txtApellidoSolicitante.Text = SolicitanteSeleccionado.Apellido;
            this.txtMailSolicitante.Text = SolicitanteSeleccionado.Mail;
        }

        private void GuardarSolicitante()
        {
            if (ModoVentanaSolicitantes == ModosVentanaSolicitante.Alta)
            {
                Solicitante nuevoSolicitante = new Solicitante()
                {
                    Nombre = this.txtNombreSolicitante.Text,
                    Apellido = this.txtApellidoSolicitante.Text,
                    Mail = this.txtMailSolicitante.Text,
                };

                nuevoSolicitante.Validate();

                ListaSolicitantes.Add(nuevoSolicitante);
            }

            if (ModoVentanaSolicitantes == ModosVentanaSolicitante.Edicion)
            {
                this.SolicitanteSeleccionado.Nombre = this.txtNombreSolicitante.Text;
                this.SolicitanteSeleccionado.Apellido = this.txtApellidoSolicitante.Text;
                this.SolicitanteSeleccionado.Mail = this.txtMailSolicitante.Text;
            }

            if (ModoVentanaSolicitantes == ModosVentanaSolicitante.Consulta)
            {
                throw new Exception(Mensajes.AccionNoValida);
            }

            this.configuracionesLogic.SaveListOfEntities<Solicitante>(ListaSolicitantes);

            this.btnGuardarSolicitante.Enabled = false; 

            CargarSolicitantes();

            this.ModoVentanaSolicitantes = ModosVentanaSolicitante.Consulta;
        }

        private void EliminarSolicitante()
        {
            var confirmResult = MessageBox.Show(Mensajes.ConfirmaEliminacion + SolicitanteSeleccionado.NombreCompleto, Mensajes.Confirmacion, MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.Yes)
            {
                ListaSolicitantes.Remove(SolicitanteSeleccionado);

                this.configuracionesLogic.SaveListOfEntities<Solicitante>(ListaSolicitantes);

                CargarSolicitantes();
            }

            ModoVentanaSolicitantes = ModosVentanaSolicitante.Consulta;
        }

        private void HabilitarModoAltaVentanaSolicitantes()
        {
            this.ModoVentanaSolicitantes = ModosVentanaSolicitante.Alta;
        }

        private void HabilitarModoEdicionVentanaSolicitantes()
        {
            this.ModoVentanaSolicitantes = ModosVentanaSolicitante.Edicion;
        }

        #endregion

        #region Events

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Escape))
            {
                Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void btnGuardarSolicitante_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarSolicitante();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevoSolicitante_Click(object sender, EventArgs e)
        {
            HabilitarModoAltaVentanaSolicitantes();
        }

        private void btnEditarSolicitante_Click(object sender, EventArgs e)
        {
            HabilitarModoEdicionVentanaSolicitantes();
        }

        private void lstSolicitantes_SelectedValueChanged(object sender, EventArgs e)
        {
            if (HayItemsSeleccionadosEnListaSolicitantes)
            {
                ModoVentanaSolicitantes = ModosVentanaSolicitante.Consulta;

                this.btnEditarSolicitante.Enabled = true;

                this.btnEliminarSolicitante.Enabled = true;

                int indiceSeleccionado = listBoxSolicitantes.SelectedIndex;

                SolicitanteSeleccionado = this.ListaSolicitantes.ElementAt(indiceSeleccionado);

                MostrarDatosSolicitanteSeleccionado();
            }
        }

        private void btnEliminarSolicitante_Click(object sender, EventArgs e)
        {
            try
            {
                EliminarSolicitante();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSalirSolicitante_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Properties

        private ModosVentanaDesarrollador _ModoDesarrollador;
        private ModosVentanaDesarrollador ModoVentanaDesarrolladores
        {
            get
            {
                return this._ModoDesarrollador;
            }
            set
            {
                this._ModoDesarrollador = value;

                if (value == ModosVentanaDesarrollador.Consulta)
                {
                    this.groupBoxDatosDesarrollador.Enabled = false;

                    this.btnNuevoDesarrollador.Enabled = true;

                    this.btnGuardarDesarrollador.Enabled = false;

                    this.btnEliminarDesarrollador.Enabled = false;
                }

                if (value == ModosVentanaDesarrollador.Alta)
                {
                    LimpiarCamposDesarrollador();

                    this.groupBoxDatosDesarrollador.Enabled = true;

                    this.btnEditarDesarrollador.Enabled = false;

                    this.btnGuardarDesarrollador.Enabled = true;
                }

                if (value == ModosVentanaDesarrollador.Edicion)
                {
                    this.groupBoxDatosDesarrollador.Enabled = true;

                    this.btnGuardarDesarrollador.Enabled = true;

                    MostrarDatosDesarrolladorSeleccionado();
                }
            }
        }

        private enum ModosVentanaDesarrollador
        {
            Alta,
            Edicion,
            Consulta
        }

        public bool HayItemsSeleccionadosEnListaDesarrolladores { get { return listBoxDesarrolladores.SelectedIndex != -1 ? true : false; } }

        #endregion

        #region Methods
         

        private void CargarDesarrolladores()
        {
            CargarListaDesarrolladores();

            listBoxDesarrolladores.DataSource = ListaDesarrolladores;

            listBoxDesarrolladores.DisplayMember = "NombreCompleto";
            listBoxDesarrolladores.ClearSelected();

            this.btnEditarDesarrollador.Enabled = false;

            LimpiarCamposDesarrollador();
        }

        private void LimpiarCamposDesarrollador()
        {
            this.txtNombreDesarrollador.Text = String.Empty;
            this.txtApellidoDesarrollador.Text = String.Empty;
            this.txtMailDesarrollador.Text = String.Empty;
        }

        private void CargarListaDesarrolladores()
        {
            ListaDesarrolladores = this.configuracionesLogic.GetListOfEntities<Desarrollador>();
        }

        private void MostrarDatosDesarrolladorSeleccionado()
        {
            this.txtNombreDesarrollador.Text = DesarrolladorSeleccionado.Nombre;
            this.txtApellidoDesarrollador.Text = DesarrolladorSeleccionado.Apellido;
            this.txtMailDesarrollador.Text = DesarrolladorSeleccionado.Mail;
        }

        private void GuardarDesarrollador()
        {
            if (ModoVentanaDesarrolladores == ModosVentanaDesarrollador.Alta)
            {
                Desarrollador nuevoDesarrollador = new Desarrollador()
                {
                    Nombre = this.txtNombreDesarrollador.Text,
                    Apellido = this.txtApellidoDesarrollador.Text,
                    Mail = this.txtMailDesarrollador.Text,
                };

                nuevoDesarrollador.Validate();

                ListaDesarrolladores.Add(nuevoDesarrollador);
            }

            if (ModoVentanaDesarrolladores == ModosVentanaDesarrollador.Edicion)
            {
                this.DesarrolladorSeleccionado.Nombre = this.txtNombreDesarrollador.Text;
                this.DesarrolladorSeleccionado.Apellido = this.txtApellidoDesarrollador.Text;
                this.DesarrolladorSeleccionado.Mail = this.txtMailDesarrollador.Text;
            }

            if (ModoVentanaDesarrolladores == ModosVentanaDesarrollador.Consulta)
            {
                throw new Exception(Mensajes.AccionNoValida);
            }

            this.configuracionesLogic.SaveListOfEntities<Desarrollador>(ListaDesarrolladores);

            this.btnGuardarDesarrollador.Enabled = false; 

            CargarDesarrolladores();

            this.ModoVentanaDesarrolladores = ModosVentanaDesarrollador.Consulta;
        }

        private void EliminarDesarrollador()
        {
            var confirmResult = MessageBox.Show(Mensajes.ConfirmaEliminacion + DesarrolladorSeleccionado.NombreCompleto, Mensajes.Confirmacion, MessageBoxButtons.YesNo);

            if (confirmResult == DialogResult.Yes)
            {
                ListaDesarrolladores.Remove(DesarrolladorSeleccionado);

                this.configuracionesLogic.SaveListOfEntities<Desarrollador>(ListaDesarrolladores);

                CargarDesarrolladores();
            }

            ModoVentanaDesarrolladores = ModosVentanaDesarrollador.Consulta;
        }

        private void HabilitarModoAltaVentanaDesarrolladores()
        {
            this.ModoVentanaDesarrolladores = ModosVentanaDesarrollador.Alta;
        }

        private void HabilitarModoEdicionVentanaDesarrolladores()
        {
            this.ModoVentanaDesarrolladores = ModosVentanaDesarrollador.Edicion;
        }

        #endregion

        #region Events
         
        private void btnGuardarDesarrollador_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarDesarrollador();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnNuevoDesarrollador_Click(object sender, EventArgs e)
        {
            HabilitarModoAltaVentanaDesarrolladores();
        }

        private void btnEditarDesarrollador_Click(object sender, EventArgs e)
        {
            HabilitarModoEdicionVentanaDesarrolladores();
        }

        private void listBoxDesarrolladores_SelectedValueChanged(object sender, EventArgs e)
        {
            if (HayItemsSeleccionadosEnListaDesarrolladores)
            {
                ModoVentanaDesarrolladores = ModosVentanaDesarrollador.Consulta;

                this.btnEditarDesarrollador.Enabled = true;

                this.btnEliminarDesarrollador.Enabled = true;

                int indiceSeleccionado = listBoxDesarrolladores.SelectedIndex;

                DesarrolladorSeleccionado = this.ListaDesarrolladores.ElementAt(indiceSeleccionado);

                MostrarDatosDesarrolladorSeleccionado();
            }
        }

        private void btnEliminarDesarrollador_Click(object sender, EventArgs e)
        {
            try
            {
                EliminarDesarrollador();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSalirDesarrollador_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion  
    }
}
