﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Entidades
{
    public class RARFile
    {
        public string nombreRar { get; set; }
        public string rutaCarpetaOrigen { get; set; }
        public string rutaDestino { get; set; }
    }
}
