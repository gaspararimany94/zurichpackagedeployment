﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Entidades
{
    public class DatosExcel : DescripcionPaquete
    {
        //TODO
        public DateTime fechaSolicitud = DateTime.Today.Date;
        public DateTime fechaImplementacion = DateTime.Today.Date;

        public List<string> listaModulos;
        public string responsablePaquete { get; set; }
        public string ambienteDestino { get; set; }
    }
}
