﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Forms;
using PackageDeployment.Logica;
using PackageDeployment.Entidades;
using PackageDeployment.Utiles;

namespace PackageDeployment.UI
{
    public partial class frmPrincipal : Form
    {
        // static DescripcionPaquete descPaquete {get; set;}
        public DescripcionPaqueteLogic descPaqueteLogic;
        public ModulosLogic modulosLogic;

        #region Ctor

        public frmPrincipal()
        {
            InitializeComponent();
            descPaqueteLogic = new DescripcionPaqueteLogic();
            modulosLogic = new ModulosLogic();
            this.CargarComboAmbiente();
            RealizarValidacionesPrevias();
        }

        #endregion

        #region Metodos

        private void CargarComboAmbiente()
        {
            List<string> ambientes = new List<string>();
            foreach (AmbienteDestino a in Ambientes.ambientesList)
            {
                ambientes.Add(a.AMBIENTEDESTINO);
            }
            cbDestino.DataSource = ambientes;
            lblOrigen.Text = this.GetCurrentAmbienteOrig(cbDestino.SelectedItem.ToString());
        }

        private void RealizarValidacionesPrevias()
        {
            Catalogo catalogo = new Catalogo();
            catalogo.CreateCarpetasLocalesFaltantes();
            bool acceso = catalogo.ComprobarExistenciaServidor(ConfigurationManager.AppSettings["rutaSource"]);
            if (!acceso)
            {
                MessageBox.Show(Mensajes.ErrorAcceso229, "FATAL ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private bool HabilitarRayo()
        {
            if (!DatosCompletosFrmDescripcion()) return false;
            if (!DatosCompletosFrmPrincipal()) return false;
            return true;
        }

        private bool DatosCompletosFrmDescripcion()
        {
            if (descPaqueteLogic.descPaquete.descripcionCambio != null || descPaqueteLogic.descPaquete.desarrollador != null
            || descPaqueteLogic.descPaquete.solicitante != null) return true;
            else return false;
        }

        private bool DatosCompletosFrmPrincipal()
        {
            if (descPaqueteLogic.descPaquete.nro == 0 || descPaqueteLogic.descPaquete.tipo == null)
                return false;
            else return true;
        }

        private void MappearDatosAPrincipal()
        {
            this.lblNro.Text = descPaqueteLogic.descPaquete.nro.ToString();
            this.lblDesarrolladorNombre.Text = descPaqueteLogic.descPaquete.desarrollador;
            this.lblSolicitanteNombre.Text = descPaqueteLogic.descPaquete.solicitante;
            this.lblDesc.Text = String.Join("\n", descPaqueteLogic.descPaquete.descripcionCambio);
            string consideracionesPrevias = String.Join("\n", descPaqueteLogic.descPaquete.consideracionesPrevias);
            this.lblConsideraciones.Text = consideracionesPrevias;
        }

        private void MostrarLabels()
        {
            string consideracionesPrevias = String.Join("\n", descPaqueteLogic.descPaquete.consideracionesPrevias);
            if (consideracionesPrevias.Trim() != String.Empty)
            {
                this.lblCP.Visible = true;
                this.lblConsideraciones.Visible = true;
            }
            else this.lblCP.Visible = false;
            if (descPaqueteLogic.descPaquete.solicitante.Trim() != String.Empty)
                this.lblSolicitanteNombre.Visible = true;
            else this.lblSolicitanteNombre.Visible = false;
            if (descPaqueteLogic.descPaquete.desarrollador.Trim() != String.Empty)
                this.lblDesarrolladorNombre.Visible = true;
            else this.lblDesarrolladorNombre.Visible = false;
            string desc = String.Join("\n", descPaqueteLogic.descPaquete.descripcionCambio);
            if (desc != String.Empty)
                this.lblDesc.Visible = true;
            else this.lblDesc.Visible = false;
        }

        private void ActualizarMods()
        {
            BindingSource binding = new BindingSource();
            binding.DataSource = modulosLogic.modulos.selectedModules;
            this.lbSelectedModules.DataSource = binding;
        }

        private string CrearNombrePaquete()
        {
            PaqueteLogic paqLogic = new PaqueteLogic(descPaqueteLogic, modulosLogic);
            return paqLogic.CrearNombrePaquete();
        }

        private List<string> AgregarSubModulos(List<string> parts, string baseFolder)
        {
            if (Contiene(baseFolder + "." + "Host")) parts.Add("HOST");
            if (Contiene("Web"))
            {
                parts.Add("WEB");
                if (Contiene("MVC")) parts.Add("MVC");
            }
            return parts;
        }

        private bool Contiene(string mod)
        {
            if (modulosLogic.modulos.selectedModules.Any(m => m.Contains(mod))) return true;
            else return false;
        }

        public string GetCurrentAmbienteOrig(string dest)
        {
            return Ambientes.ambientesList.Where(s => s.AMBIENTEDESTINO == dest)
                                                    .Select(i => i.AMBIENTEORIGEN).FirstOrDefault().ToString();
        }

        public string GetCurrentAmbienteDest(string orig)
        {
            return Ambientes.ambientesList.Where(s => s.AMBIENTEORIGEN == orig)
                                                    .Select(i => i.AMBIENTEDESTINO).FirstOrDefault().ToString();
        }

        #endregion        

        #region Eventos

        private void btnComprimir_Click(object sender, EventArgs e)
        {
            if (HabilitarRayo())
            {
                RARFileLogic rarFile = new RARFileLogic();
                try
                {
                    string ambienteOrigen = lblOrigen.Text;
                    rarFile.ComprimirFiles(CrearNombrePaquete(), ambienteOrigen);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Mensajes.ErrorComprimir, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else MessageBox.Show(Mensajes.FaltanDatosObligatorios, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }        

        private void crearExcel()
        {
            ExcelFileLogic efl = new ExcelFileLogic();
            try
            {
                string destino = cbDestino.SelectedItem.ToString();
                efl.CopiarExcel(destino);
                DatosExcel de = new DatosExcel();
                if (rbTicket.Checked)
                {
                    de.nro = int.Parse(tbMaskNumero.Text);
                }
                else
                {
                    de.nro = int.Parse(tbMaskNumero.Text);
                }
                de.ambienteDestino = destino;
                de.desarrollador = lblDesarrolladorNombre.Text;
                de.solicitante = lblSolicitanteNombre.Text;
                de.descripcionCambio = new[] { lblDesc.Text };
                de.listaModulos = new List<string>();
                foreach (string modulo in modulosLogic.modulos.selectedModules)
                {
                    de.listaModulos.Add(modulo);
                }
                efl.CompletarDatosExcel(de);
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, Mensajes.ErrorExcel, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDescripcion_Click(object sender, EventArgs e)
        {
            frmDescripcion frmDesc = new frmDescripcion(descPaqueteLogic);
            frmDesc.ShowDialog();
            if (descPaqueteLogic.descPaquete.sePuedeEditar)
            {
                this.MappearDatosAPrincipal();
                this.MostrarLabels();
            }
        }        

        private void rbTicket_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTicket.Checked)
            {
                rbIniciativa.Checked = false;
                descPaqueteLogic.SetTipo(Constantes.TICKET);
                lblImplementacion.Text = Constantes.TICKET + ":";
            }
        }

        private void rbIniciativa_CheckedChanged(object sender, EventArgs e)
        {
            if (rbIniciativa.Checked)
            {
                rbTicket.Checked = false;
                descPaqueteLogic.SetTipo(Constantes.INICIATIVA);
                lblImplementacion.Text = Constantes.INICIATIVA + ":";
            }
        }

        private void tbMaskNumero_Leave(object sender, EventArgs e)
        {
            try
            {
                if (tbMaskNumero.Text.Trim() != String.Empty)
                {
                    descPaqueteLogic.SetNro(int.Parse(tbMaskNumero.Text));
                    this.lblNro.Text = this.tbMaskNumero.Text.Trim();
                    this.lblNro.Visible = true;
                }
                else this.lblNro.Visible = false;
            }
            catch
            {
                string mensajeError = String.Format(Mensajes.NroIncorrecto, descPaqueteLogic.GetTipo());
                MessageBox.Show(mensajeError, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tbMaskNumero.Clear();
            }
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            frmConfiguraciones frmConfiguraciones = new frmConfiguraciones();
            frmConfiguraciones.ShowDialog();
        }

        private void btnSeleccionarModulos_Click(object sender, EventArgs e)
        {
            modulosLogic.SetAmbiente(lblOrigen.Text);
            frmModulos frmMods = new frmModulos(modulosLogic);
            frmMods.ShowDialog();
            this.ActualizarMods();
        }        

        private void frmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void cbDestino_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblOrigen.Text = this.GetCurrentAmbienteOrig(cbDestino.SelectedItem.ToString());
            modulosLogic.ClearModulos();
            this.ActualizarMods();
        }

        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            crearExcel();
        }
    }
}
