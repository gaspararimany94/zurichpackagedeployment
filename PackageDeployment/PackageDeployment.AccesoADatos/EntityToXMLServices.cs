﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Configuration;

namespace PackageDeployment.AccesoADatos
{
    public class EntityToXMLServices : IEntityToXmlServices
    {
        public string XML_PathPackageDeployment;
        public static String XMLExtension = ".xml";

        public EntityToXMLServices()
        {
            var route = ConfigurationManager.AppSettings["XMLPathPackageDeployment"];

            XML_PathPackageDeployment = route.Replace("[User]", Environment.UserName);
        }

        public void SaveListOfEntities<T>(object obj)
        {
            String entityFileKey = typeof(T).Name;

            string filePath = XML_PathPackageDeployment + entityFileKey + XMLExtension;
            XmlSerializer sr = new XmlSerializer(obj.GetType());
            TextWriter writer = new StreamWriter(filePath);
            sr.Serialize(writer, obj);
            writer.Close();
        }

        public List<T> GetListOfEntities<T>()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<T>));

            string ruta = XML_PathPackageDeployment + typeof(T).Name + XMLExtension;

            FileStream stream = new FileStream(ruta, FileMode.Open, FileAccess.Read, FileShare.Read);

            List<T> deserializedList = (List<T>)serializer.Deserialize(stream);

            stream.Close();

            return deserializedList;
        }
    }
}
