﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PackageDeployment.Utiles;

namespace PackageDeployment.Entidades
{
    public class DescripcionPaquete
    {
        public DescripcionPaquete()
        {
            this.sePuedeEditar = false;
            this.tipo = Constantes.TICKET;
        }

        private static DescripcionPaquete _paquete;

        public static DescripcionPaquete Paquete
        {
            get
            {
                if (_paquete == null)
                    _paquete = new DescripcionPaquete();
                return _paquete;
            }
            set { _paquete = value; }
        }

        public string tipo { get; set; }
        public int nro { get; set; }
        public string desarrollador { get; set; }
        public string solicitante { get; set; }
        public string[] descripcionCambio { get; set; }
        public string[] consideracionesPrevias { get; set; }
        public bool sePuedeEditar { get; set; }
    }
}
