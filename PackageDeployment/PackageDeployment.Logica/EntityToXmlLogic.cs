﻿using PackageDeployment.AccesoADatos;
using PackageDeployment.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Logica
{
    public class EntityToXMLLogic : IEntityToXMLLogic
    {
        private readonly IEntityToXmlServices xmlService;

        public EntityToXMLLogic()
        {
            this.xmlService = new EntityToXMLServices();
        }

        public List<T> GetListOfEntities<T>()
        {
            return this.xmlService.GetListOfEntities<T>();
        }

        public void SaveListOfEntities<T>(List<T> list)
        {
            Validate<T>(list);

            Type t = typeof(T);

            this.xmlService.SaveListOfEntities<T>(list);
        }

        private void Validate<T>(List<T> list)
        {
            foreach (T item in list)
            {
                (item as PackageDeploymentEntity).Validate();
            }
        } 
    }
}
