﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PackageDeployment.Entidades;

namespace PackageDeployment.Logica
{
    public class DescripcionPaqueteLogic
    {
        public DescripcionPaquete descPaquete {get; set;}// = new DescripcionPaquete();
        public DescripcionPaqueteLogic()
        {
            this.descPaquete = new DescripcionPaquete();
        }

        public void cargarDatosDescripcion(int nro, string desarrollador, string solicitante, string[] descCambio, string[] consPrevias)
        {
            descPaquete.nro = nro;
            descPaquete.desarrollador = desarrollador;
            descPaquete.solicitante = solicitante;
            descPaquete.descripcionCambio = descCambio;
            descPaquete.consideracionesPrevias = consPrevias;
        }

        public void SetTipo(string tipo)
        {
            this.descPaquete.tipo = tipo;
        }

        public void SetNro(int nro)
        {
            this.descPaquete.nro = nro;
        }

        public string GetTipo()
        {
            return this.descPaquete.tipo;
        }

        public int GetNro()
        {
            return this.descPaquete.nro;
        }
    }
}
