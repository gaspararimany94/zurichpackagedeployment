﻿using PackageDeployment.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackageDeployment.Logica
{
    public interface IEntityToXMLLogic
    {
        List<T> GetListOfEntities<T>();

        void SaveListOfEntities<T>(List<T> list);
    }
}
