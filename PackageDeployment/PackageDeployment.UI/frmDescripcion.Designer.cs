﻿namespace PackageDeployment.UI
{
    partial class frmDescripcion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbSolicitante = new System.Windows.Forms.ComboBox();
            this.cbDesarrollador = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbDescripcionCambio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbConsideraciones = new System.Windows.Forms.TextBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Solicitante:";
            // 
            // cbSolicitante
            // 
            this.cbSolicitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSolicitante.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbSolicitante.FormattingEnabled = true;
            this.cbSolicitante.Location = new System.Drawing.Point(79, 13);
            this.cbSolicitante.Name = "cbSolicitante";
            this.cbSolicitante.Size = new System.Drawing.Size(121, 21);
            this.cbSolicitante.TabIndex = 1;
            // 
            // cbDesarrollador
            // 
            this.cbDesarrollador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDesarrollador.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbDesarrollador.FormattingEnabled = true;
            this.cbDesarrollador.Location = new System.Drawing.Point(366, 12);
            this.cbDesarrollador.Name = "cbDesarrollador";
            this.cbDesarrollador.Size = new System.Drawing.Size(121, 21);
            this.cbDesarrollador.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label2.Location = new System.Drawing.Point(288, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Desarrollador:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Descripcion del cambio:";
            // 
            // tbDescripcionCambio
            // 
            this.tbDescripcionCambio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbDescripcionCambio.Location = new System.Drawing.Point(17, 77);
            this.tbDescripcionCambio.Multiline = true;
            this.tbDescripcionCambio.Name = "tbDescripcionCambio";
            this.tbDescripcionCambio.Size = new System.Drawing.Size(470, 80);
            this.tbDescripcionCambio.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Consideraciones previas: (Opcional)";
            // 
            // tbConsideraciones
            // 
            this.tbConsideraciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbConsideraciones.Location = new System.Drawing.Point(17, 184);
            this.tbConsideraciones.Multiline = true;
            this.tbConsideraciones.Name = "tbConsideraciones";
            this.tbConsideraciones.Size = new System.Drawing.Size(470, 80);
            this.tbConsideraciones.TabIndex = 7;
            // 
            // btnAceptar
            // 
            this.btnAceptar.BackColor = System.Drawing.Color.LightGray;
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAceptar.Location = new System.Drawing.Point(411, 280);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 8;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // frmDescripcion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(510, 315);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.tbConsideraciones);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbDescripcionCambio);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbDesarrollador);
            this.Controls.Add(this.cbSolicitante);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmDescripcion";
            this.Text = "Descripcion del cambio";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbSolicitante;
        private System.Windows.Forms.ComboBox cbDesarrollador;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbDescripcionCambio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbConsideraciones;
        private System.Windows.Forms.Button btnAceptar;
    }
}