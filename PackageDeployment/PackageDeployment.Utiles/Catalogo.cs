﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

namespace PackageDeployment.Utiles
{
    public class Catalogo
    {
        public bool ComprobarExistenciaServidor(string ruta)
        {
            int longitud = ruta.Length;
            if (ruta[longitud - 1].ToString() == "\\") return false;       
            else if (!System.IO.Directory.Exists(ruta)) return false;
            return true;
        }
        
        public int ObtenerNroPaquete(string ambiente)
        {
            int nroPaquete = int.Parse(NroPaquete.ResourceManager.GetString(ambiente));
            ActualizarNroPaquete(ambiente, nroPaquete);
            return nroPaquete;
        }

        private void ActualizarNroPaquete(string ambiente, int numeroPaqueteAnterior)
        {
            string path = Directory.GetCurrentDirectory();
            path = path.Replace(@"PackageDeployment.UI\bin\Debug", @"PackageDeployment.Utiles\NroPaquete.resx");

            Hashtable resourceEntries = new Hashtable();
            Hashtable data = new Hashtable();
            numeroPaqueteAnterior += 1;
            data.Add(ambiente, numeroPaqueteAnterior);

            //Get existing resources
            ResXResourceReader reader = new ResXResourceReader(path);

            if (reader != null)
            {
                IDictionaryEnumerator id = reader.GetEnumerator();
                foreach (DictionaryEntry d in reader)
                {
                    if (d.Value == null)
                        resourceEntries.Add(d.Key.ToString(), "");
                    else
                        resourceEntries.Add(d.Key.ToString(), d.Value.ToString());
                }
                reader.Close();
            }

            //Modify resources here...
            foreach (String key in data.Keys)
            {
                if (resourceEntries.ContainsKey(key))
                {

                    String value = data[key].ToString();
                    if (value == null) value = "";
                    resourceEntries.Remove(key);
                    resourceEntries.Add(key, value);
                }
            }

            //Write the combined resource file
            ResXResourceWriter resourceWriter = new ResXResourceWriter(path);

            foreach (String key in resourceEntries.Keys)
            {
                resourceWriter.AddResource(key, resourceEntries[key]);
            }
            resourceWriter.Generate();
            resourceWriter.Close();
        }

        public void CreateCarpetasLocalesFaltantes()
        {
            string rutaPaquetes = ConfigurationManager.AppSettings["rutaPackage"];
            List<string> carpetasLocales = new List<string>();

            carpetasLocales.Add(CarpetasDefault.CARPETADESALOCAL);
            carpetasLocales.Add(CarpetasDefault.CARPETAPREPRODLOCAL);
            carpetasLocales.Add(CarpetasDefault.CARPETATESTLOCAL);
            carpetasLocales.Add(CarpetasDefault.CARPETAPRODUCCIONLOCAL);

            rutaPaquetes = rutaPaquetes.Replace("{0}", DateTime.Now.Year.ToString());

            Directory.CreateDirectory(rutaPaquetes);
            foreach (string carpetas in carpetasLocales)
            {
                Directory.CreateDirectory(rutaPaquetes + @"\" + carpetas);
            }
        }
    }
}
