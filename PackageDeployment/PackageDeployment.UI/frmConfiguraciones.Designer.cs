﻿namespace PackageDeployment.UI
{
    partial class frmConfiguraciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcConfiguraciones = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBoxAccionesSolicitante = new System.Windows.Forms.GroupBox();
            this.btnEliminarSolicitante = new System.Windows.Forms.Button();
            this.btnSalirSolicitante = new System.Windows.Forms.Button();
            this.btnGuardarSolicitante = new System.Windows.Forms.Button();
            this.btnEditarSolicitante = new System.Windows.Forms.Button();
            this.btnNuevoSolicitante = new System.Windows.Forms.Button();
            this.groupBoxDatosSolicitante = new System.Windows.Forms.GroupBox();
            this.txtMailSolicitante = new System.Windows.Forms.TextBox();
            this.txtApellidoSolicitante = new System.Windows.Forms.TextBox();
            this.txtNombreSolicitante = new System.Windows.Forms.TextBox();
            this.lblMailSolicitante = new System.Windows.Forms.Label();
            this.lblApellidoSolicitante = new System.Windows.Forms.Label();
            this.lblNombreSolicitante = new System.Windows.Forms.Label();
            this.groupListaSolicitantes = new System.Windows.Forms.GroupBox();
            this.listBoxSolicitantes = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBoxAccionesDesarrollador = new System.Windows.Forms.GroupBox();
            this.btnEliminarDesarrollador = new System.Windows.Forms.Button();
            this.btnSalirDesarrollador = new System.Windows.Forms.Button();
            this.btnGuardarDesarrollador = new System.Windows.Forms.Button();
            this.btnEditarDesarrollador = new System.Windows.Forms.Button();
            this.btnNuevoDesarrollador = new System.Windows.Forms.Button();
            this.groupBoxDatosDesarrollador = new System.Windows.Forms.GroupBox();
            this.txtMailDesarrollador = new System.Windows.Forms.TextBox();
            this.txtApellidoDesarrollador = new System.Windows.Forms.TextBox();
            this.txtNombreDesarrollador = new System.Windows.Forms.TextBox();
            this.lblMailDesarrollador = new System.Windows.Forms.Label();
            this.lblApellidoDesarrollador = new System.Windows.Forms.Label();
            this.lblNombreDesarrollador = new System.Windows.Forms.Label();
            this.groupBoxListaDesarrolladores = new System.Windows.Forms.GroupBox();
            this.listBoxDesarrolladores = new System.Windows.Forms.ListBox();
            this.tbcConfiguraciones.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBoxAccionesSolicitante.SuspendLayout();
            this.groupBoxDatosSolicitante.SuspendLayout();
            this.groupListaSolicitantes.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBoxAccionesDesarrollador.SuspendLayout();
            this.groupBoxDatosDesarrollador.SuspendLayout();
            this.groupBoxListaDesarrolladores.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbcConfiguraciones
            // 
            this.tbcConfiguraciones.Controls.Add(this.tabPage1);
            this.tbcConfiguraciones.Controls.Add(this.tabPage2);
            this.tbcConfiguraciones.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbcConfiguraciones.Location = new System.Drawing.Point(0, 0);
            this.tbcConfiguraciones.Name = "tbcConfiguraciones";
            this.tbcConfiguraciones.SelectedIndex = 0;
            this.tbcConfiguraciones.Size = new System.Drawing.Size(549, 475);
            this.tbcConfiguraciones.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBoxAccionesSolicitante);
            this.tabPage1.Controls.Add(this.groupBoxDatosSolicitante);
            this.tabPage1.Controls.Add(this.groupListaSolicitantes);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(541, 449);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Solicitantes";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBoxAccionesSolicitante
            // 
            this.groupBoxAccionesSolicitante.Controls.Add(this.btnEliminarSolicitante);
            this.groupBoxAccionesSolicitante.Controls.Add(this.btnSalirSolicitante);
            this.groupBoxAccionesSolicitante.Controls.Add(this.btnGuardarSolicitante);
            this.groupBoxAccionesSolicitante.Controls.Add(this.btnEditarSolicitante);
            this.groupBoxAccionesSolicitante.Controls.Add(this.btnNuevoSolicitante);
            this.groupBoxAccionesSolicitante.Location = new System.Drawing.Point(14, 374);
            this.groupBoxAccionesSolicitante.Name = "groupBoxAccionesSolicitante";
            this.groupBoxAccionesSolicitante.Size = new System.Drawing.Size(521, 54);
            this.groupBoxAccionesSolicitante.TabIndex = 2;
            this.groupBoxAccionesSolicitante.TabStop = false;
            this.groupBoxAccionesSolicitante.Text = "Acciones";
            // 
            // btnEliminarSolicitante
            // 
            this.btnEliminarSolicitante.BackColor = System.Drawing.Color.LightGray;
            this.btnEliminarSolicitante.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEliminarSolicitante.Location = new System.Drawing.Point(171, 19);
            this.btnEliminarSolicitante.Name = "btnEliminarSolicitante";
            this.btnEliminarSolicitante.Size = new System.Drawing.Size(75, 23);
            this.btnEliminarSolicitante.TabIndex = 4;
            this.btnEliminarSolicitante.Text = "Eliminar";
            this.btnEliminarSolicitante.UseVisualStyleBackColor = false;
            this.btnEliminarSolicitante.Click += new System.EventHandler(this.btnEliminarSolicitante_Click);
            // 
            // btnSalirSolicitante
            // 
            this.btnSalirSolicitante.BackColor = System.Drawing.Color.LightGray;
            this.btnSalirSolicitante.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSalirSolicitante.Location = new System.Drawing.Point(440, 19);
            this.btnSalirSolicitante.Name = "btnSalirSolicitante";
            this.btnSalirSolicitante.Size = new System.Drawing.Size(75, 23);
            this.btnSalirSolicitante.TabIndex = 3;
            this.btnSalirSolicitante.Text = "Salir";
            this.btnSalirSolicitante.UseVisualStyleBackColor = false;
            this.btnSalirSolicitante.Click += new System.EventHandler(this.btnSalirSolicitante_Click);
            // 
            // btnGuardarSolicitante
            // 
            this.btnGuardarSolicitante.BackColor = System.Drawing.Color.LightGray;
            this.btnGuardarSolicitante.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardarSolicitante.Location = new System.Drawing.Point(359, 19);
            this.btnGuardarSolicitante.Name = "btnGuardarSolicitante";
            this.btnGuardarSolicitante.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarSolicitante.TabIndex = 2;
            this.btnGuardarSolicitante.Text = "Guardar";
            this.btnGuardarSolicitante.UseVisualStyleBackColor = false;
            this.btnGuardarSolicitante.Click += new System.EventHandler(this.btnGuardarSolicitante_Click);
            // 
            // btnEditarSolicitante
            // 
            this.btnEditarSolicitante.BackColor = System.Drawing.Color.LightGray;
            this.btnEditarSolicitante.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEditarSolicitante.Location = new System.Drawing.Point(90, 19);
            this.btnEditarSolicitante.Name = "btnEditarSolicitante";
            this.btnEditarSolicitante.Size = new System.Drawing.Size(75, 23);
            this.btnEditarSolicitante.TabIndex = 1;
            this.btnEditarSolicitante.Text = "Editar";
            this.btnEditarSolicitante.UseVisualStyleBackColor = false;
            this.btnEditarSolicitante.Click += new System.EventHandler(this.btnEditarSolicitante_Click);
            // 
            // btnNuevoSolicitante
            // 
            this.btnNuevoSolicitante.BackColor = System.Drawing.Color.LightGray;
            this.btnNuevoSolicitante.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNuevoSolicitante.Location = new System.Drawing.Point(9, 19);
            this.btnNuevoSolicitante.Name = "btnNuevoSolicitante";
            this.btnNuevoSolicitante.Size = new System.Drawing.Size(75, 23);
            this.btnNuevoSolicitante.TabIndex = 0;
            this.btnNuevoSolicitante.Text = "Nuevo";
            this.btnNuevoSolicitante.UseVisualStyleBackColor = false;
            this.btnNuevoSolicitante.Click += new System.EventHandler(this.btnNuevoSolicitante_Click);
            // 
            // groupBoxDatosSolicitante
            // 
            this.groupBoxDatosSolicitante.Controls.Add(this.txtMailSolicitante);
            this.groupBoxDatosSolicitante.Controls.Add(this.txtApellidoSolicitante);
            this.groupBoxDatosSolicitante.Controls.Add(this.txtNombreSolicitante);
            this.groupBoxDatosSolicitante.Controls.Add(this.lblMailSolicitante);
            this.groupBoxDatosSolicitante.Controls.Add(this.lblApellidoSolicitante);
            this.groupBoxDatosSolicitante.Controls.Add(this.lblNombreSolicitante);
            this.groupBoxDatosSolicitante.Location = new System.Drawing.Point(14, 255);
            this.groupBoxDatosSolicitante.Name = "groupBoxDatosSolicitante";
            this.groupBoxDatosSolicitante.Size = new System.Drawing.Size(521, 113);
            this.groupBoxDatosSolicitante.TabIndex = 1;
            this.groupBoxDatosSolicitante.TabStop = false;
            this.groupBoxDatosSolicitante.Text = "Datos Solicitante";
            // 
            // txtMailSolicitante
            // 
            this.txtMailSolicitante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMailSolicitante.Location = new System.Drawing.Point(101, 78);
            this.txtMailSolicitante.Name = "txtMailSolicitante";
            this.txtMailSolicitante.Size = new System.Drawing.Size(402, 20);
            this.txtMailSolicitante.TabIndex = 6;
            // 
            // txtApellidoSolicitante
            // 
            this.txtApellidoSolicitante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApellidoSolicitante.Location = new System.Drawing.Point(101, 52);
            this.txtApellidoSolicitante.Name = "txtApellidoSolicitante";
            this.txtApellidoSolicitante.Size = new System.Drawing.Size(402, 20);
            this.txtApellidoSolicitante.TabIndex = 5;
            // 
            // txtNombreSolicitante
            // 
            this.txtNombreSolicitante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombreSolicitante.Location = new System.Drawing.Point(101, 22);
            this.txtNombreSolicitante.Name = "txtNombreSolicitante";
            this.txtNombreSolicitante.Size = new System.Drawing.Size(402, 20);
            this.txtNombreSolicitante.TabIndex = 4;
            // 
            // lblMailSolicitante
            // 
            this.lblMailSolicitante.AutoSize = true;
            this.lblMailSolicitante.Location = new System.Drawing.Point(6, 80);
            this.lblMailSolicitante.Name = "lblMailSolicitante";
            this.lblMailSolicitante.Size = new System.Drawing.Size(26, 13);
            this.lblMailSolicitante.TabIndex = 3;
            this.lblMailSolicitante.Text = "Mail";
            // 
            // lblApellidoSolicitante
            // 
            this.lblApellidoSolicitante.AutoSize = true;
            this.lblApellidoSolicitante.Location = new System.Drawing.Point(6, 52);
            this.lblApellidoSolicitante.Name = "lblApellidoSolicitante";
            this.lblApellidoSolicitante.Size = new System.Drawing.Size(44, 13);
            this.lblApellidoSolicitante.TabIndex = 2;
            this.lblApellidoSolicitante.Text = "Apellido";
            // 
            // lblNombreSolicitante
            // 
            this.lblNombreSolicitante.AutoSize = true;
            this.lblNombreSolicitante.Location = new System.Drawing.Point(6, 25);
            this.lblNombreSolicitante.Name = "lblNombreSolicitante";
            this.lblNombreSolicitante.Size = new System.Drawing.Size(44, 13);
            this.lblNombreSolicitante.TabIndex = 1;
            this.lblNombreSolicitante.Text = "Nombre";
            // 
            // groupListaSolicitantes
            // 
            this.groupListaSolicitantes.Controls.Add(this.listBoxSolicitantes);
            this.groupListaSolicitantes.Location = new System.Drawing.Point(14, 6);
            this.groupListaSolicitantes.Name = "groupListaSolicitantes";
            this.groupListaSolicitantes.Size = new System.Drawing.Size(521, 243);
            this.groupListaSolicitantes.TabIndex = 0;
            this.groupListaSolicitantes.TabStop = false;
            this.groupListaSolicitantes.Text = "Lista Solicitantes";
            // 
            // listBoxSolicitantes
            // 
            this.listBoxSolicitantes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBoxSolicitantes.FormattingEnabled = true;
            this.listBoxSolicitantes.Location = new System.Drawing.Point(6, 19);
            this.listBoxSolicitantes.Name = "listBoxSolicitantes";
            this.listBoxSolicitantes.ScrollAlwaysVisible = true;
            this.listBoxSolicitantes.Size = new System.Drawing.Size(503, 210);
            this.listBoxSolicitantes.TabIndex = 0;
            this.listBoxSolicitantes.SelectedValueChanged += new System.EventHandler(this.lstSolicitantes_SelectedValueChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBoxAccionesDesarrollador);
            this.tabPage2.Controls.Add(this.groupBoxDatosDesarrollador);
            this.tabPage2.Controls.Add(this.groupBoxListaDesarrolladores);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(541, 449);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Desarrolladores";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBoxAccionesDesarrollador
            // 
            this.groupBoxAccionesDesarrollador.Controls.Add(this.btnEliminarDesarrollador);
            this.groupBoxAccionesDesarrollador.Controls.Add(this.btnSalirDesarrollador);
            this.groupBoxAccionesDesarrollador.Controls.Add(this.btnGuardarDesarrollador);
            this.groupBoxAccionesDesarrollador.Controls.Add(this.btnEditarDesarrollador);
            this.groupBoxAccionesDesarrollador.Controls.Add(this.btnNuevoDesarrollador);
            this.groupBoxAccionesDesarrollador.Location = new System.Drawing.Point(14, 374);
            this.groupBoxAccionesDesarrollador.Name = "groupBoxAccionesDesarrollador";
            this.groupBoxAccionesDesarrollador.Size = new System.Drawing.Size(521, 54);
            this.groupBoxAccionesDesarrollador.TabIndex = 3;
            this.groupBoxAccionesDesarrollador.TabStop = false;
            this.groupBoxAccionesDesarrollador.Text = "Acciones";
            // 
            // btnEliminarDesarrollador
            // 
            this.btnEliminarDesarrollador.BackColor = System.Drawing.Color.LightGray;
            this.btnEliminarDesarrollador.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEliminarDesarrollador.Location = new System.Drawing.Point(171, 19);
            this.btnEliminarDesarrollador.Name = "btnEliminarDesarrollador";
            this.btnEliminarDesarrollador.Size = new System.Drawing.Size(75, 23);
            this.btnEliminarDesarrollador.TabIndex = 4;
            this.btnEliminarDesarrollador.Text = "Eliminar";
            this.btnEliminarDesarrollador.UseVisualStyleBackColor = false;
            this.btnEliminarDesarrollador.Click += new System.EventHandler(this.btnEliminarDesarrollador_Click);
            // 
            // btnSalirDesarrollador
            // 
            this.btnSalirDesarrollador.BackColor = System.Drawing.Color.LightGray;
            this.btnSalirDesarrollador.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSalirDesarrollador.Location = new System.Drawing.Point(440, 19);
            this.btnSalirDesarrollador.Name = "btnSalirDesarrollador";
            this.btnSalirDesarrollador.Size = new System.Drawing.Size(75, 23);
            this.btnSalirDesarrollador.TabIndex = 3;
            this.btnSalirDesarrollador.Text = "Salir";
            this.btnSalirDesarrollador.UseVisualStyleBackColor = false;
            this.btnSalirDesarrollador.Click += new System.EventHandler(this.btnSalirDesarrollador_Click);
            // 
            // btnGuardarDesarrollador
            // 
            this.btnGuardarDesarrollador.BackColor = System.Drawing.Color.LightGray;
            this.btnGuardarDesarrollador.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardarDesarrollador.Location = new System.Drawing.Point(359, 19);
            this.btnGuardarDesarrollador.Name = "btnGuardarDesarrollador";
            this.btnGuardarDesarrollador.Size = new System.Drawing.Size(75, 23);
            this.btnGuardarDesarrollador.TabIndex = 2;
            this.btnGuardarDesarrollador.Text = "Guardar";
            this.btnGuardarDesarrollador.UseVisualStyleBackColor = false;
            this.btnGuardarDesarrollador.Click += new System.EventHandler(this.btnGuardarDesarrollador_Click);
            // 
            // btnEditarDesarrollador
            // 
            this.btnEditarDesarrollador.BackColor = System.Drawing.Color.LightGray;
            this.btnEditarDesarrollador.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEditarDesarrollador.Location = new System.Drawing.Point(90, 19);
            this.btnEditarDesarrollador.Name = "btnEditarDesarrollador";
            this.btnEditarDesarrollador.Size = new System.Drawing.Size(75, 23);
            this.btnEditarDesarrollador.TabIndex = 1;
            this.btnEditarDesarrollador.Text = "Editar";
            this.btnEditarDesarrollador.UseVisualStyleBackColor = false;
            this.btnEditarDesarrollador.Click += new System.EventHandler(this.btnEditarDesarrollador_Click);
            // 
            // btnNuevoDesarrollador
            // 
            this.btnNuevoDesarrollador.BackColor = System.Drawing.Color.LightGray;
            this.btnNuevoDesarrollador.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNuevoDesarrollador.Location = new System.Drawing.Point(9, 19);
            this.btnNuevoDesarrollador.Name = "btnNuevoDesarrollador";
            this.btnNuevoDesarrollador.Size = new System.Drawing.Size(75, 23);
            this.btnNuevoDesarrollador.TabIndex = 0;
            this.btnNuevoDesarrollador.Text = "Nuevo";
            this.btnNuevoDesarrollador.UseVisualStyleBackColor = false;
            this.btnNuevoDesarrollador.Click += new System.EventHandler(this.btnNuevoDesarrollador_Click);
            // 
            // groupBoxDatosDesarrollador
            // 
            this.groupBoxDatosDesarrollador.Controls.Add(this.txtMailDesarrollador);
            this.groupBoxDatosDesarrollador.Controls.Add(this.txtApellidoDesarrollador);
            this.groupBoxDatosDesarrollador.Controls.Add(this.txtNombreDesarrollador);
            this.groupBoxDatosDesarrollador.Controls.Add(this.lblMailDesarrollador);
            this.groupBoxDatosDesarrollador.Controls.Add(this.lblApellidoDesarrollador);
            this.groupBoxDatosDesarrollador.Controls.Add(this.lblNombreDesarrollador);
            this.groupBoxDatosDesarrollador.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBoxDatosDesarrollador.Location = new System.Drawing.Point(14, 255);
            this.groupBoxDatosDesarrollador.Name = "groupBoxDatosDesarrollador";
            this.groupBoxDatosDesarrollador.Size = new System.Drawing.Size(521, 113);
            this.groupBoxDatosDesarrollador.TabIndex = 2;
            this.groupBoxDatosDesarrollador.TabStop = false;
            this.groupBoxDatosDesarrollador.Text = "Datos Desarrollador";
            // 
            // txtMailDesarrollador
            // 
            this.txtMailDesarrollador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMailDesarrollador.Location = new System.Drawing.Point(101, 78);
            this.txtMailDesarrollador.Name = "txtMailDesarrollador";
            this.txtMailDesarrollador.Size = new System.Drawing.Size(402, 20);
            this.txtMailDesarrollador.TabIndex = 6;
            // 
            // txtApellidoDesarrollador
            // 
            this.txtApellidoDesarrollador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtApellidoDesarrollador.Location = new System.Drawing.Point(101, 52);
            this.txtApellidoDesarrollador.Name = "txtApellidoDesarrollador";
            this.txtApellidoDesarrollador.Size = new System.Drawing.Size(402, 20);
            this.txtApellidoDesarrollador.TabIndex = 5;
            // 
            // txtNombreDesarrollador
            // 
            this.txtNombreDesarrollador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombreDesarrollador.Location = new System.Drawing.Point(101, 22);
            this.txtNombreDesarrollador.Name = "txtNombreDesarrollador";
            this.txtNombreDesarrollador.Size = new System.Drawing.Size(402, 20);
            this.txtNombreDesarrollador.TabIndex = 4;
            // 
            // lblMailDesarrollador
            // 
            this.lblMailDesarrollador.AutoSize = true;
            this.lblMailDesarrollador.Location = new System.Drawing.Point(6, 80);
            this.lblMailDesarrollador.Name = "lblMailDesarrollador";
            this.lblMailDesarrollador.Size = new System.Drawing.Size(26, 13);
            this.lblMailDesarrollador.TabIndex = 3;
            this.lblMailDesarrollador.Text = "Mail";
            // 
            // lblApellidoDesarrollador
            // 
            this.lblApellidoDesarrollador.AutoSize = true;
            this.lblApellidoDesarrollador.Location = new System.Drawing.Point(6, 52);
            this.lblApellidoDesarrollador.Name = "lblApellidoDesarrollador";
            this.lblApellidoDesarrollador.Size = new System.Drawing.Size(44, 13);
            this.lblApellidoDesarrollador.TabIndex = 2;
            this.lblApellidoDesarrollador.Text = "Apellido";
            // 
            // lblNombreDesarrollador
            // 
            this.lblNombreDesarrollador.AutoSize = true;
            this.lblNombreDesarrollador.Location = new System.Drawing.Point(6, 25);
            this.lblNombreDesarrollador.Name = "lblNombreDesarrollador";
            this.lblNombreDesarrollador.Size = new System.Drawing.Size(44, 13);
            this.lblNombreDesarrollador.TabIndex = 1;
            this.lblNombreDesarrollador.Text = "Nombre";
            // 
            // groupBoxListaDesarrolladores
            // 
            this.groupBoxListaDesarrolladores.Controls.Add(this.listBoxDesarrolladores);
            this.groupBoxListaDesarrolladores.Location = new System.Drawing.Point(14, 6);
            this.groupBoxListaDesarrolladores.Name = "groupBoxListaDesarrolladores";
            this.groupBoxListaDesarrolladores.Size = new System.Drawing.Size(524, 243);
            this.groupBoxListaDesarrolladores.TabIndex = 1;
            this.groupBoxListaDesarrolladores.TabStop = false;
            this.groupBoxListaDesarrolladores.Text = "Lista Desarrolladores";
            // 
            // listBoxDesarrolladores
            // 
            this.listBoxDesarrolladores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBoxDesarrolladores.FormattingEnabled = true;
            this.listBoxDesarrolladores.Location = new System.Drawing.Point(6, 19);
            this.listBoxDesarrolladores.Name = "listBoxDesarrolladores";
            this.listBoxDesarrolladores.ScrollAlwaysVisible = true;
            this.listBoxDesarrolladores.Size = new System.Drawing.Size(503, 210);
            this.listBoxDesarrolladores.TabIndex = 0;
            this.listBoxDesarrolladores.SelectedValueChanged += new System.EventHandler(this.listBoxDesarrolladores_SelectedValueChanged);
            // 
            // frmConfiguraciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 487);
            this.Controls.Add(this.tbcConfiguraciones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmConfiguraciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuraciones";
            this.tbcConfiguraciones.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBoxAccionesSolicitante.ResumeLayout(false);
            this.groupBoxDatosSolicitante.ResumeLayout(false);
            this.groupBoxDatosSolicitante.PerformLayout();
            this.groupListaSolicitantes.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBoxAccionesDesarrollador.ResumeLayout(false);
            this.groupBoxDatosDesarrollador.ResumeLayout(false);
            this.groupBoxDatosDesarrollador.PerformLayout();
            this.groupBoxListaDesarrolladores.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbcConfiguraciones;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBoxAccionesSolicitante;
        private System.Windows.Forms.Button btnSalirSolicitante;
        private System.Windows.Forms.Button btnGuardarSolicitante;
        private System.Windows.Forms.Button btnNuevoSolicitante;
        private System.Windows.Forms.GroupBox groupBoxDatosSolicitante;
        private System.Windows.Forms.TextBox txtMailSolicitante;
        private System.Windows.Forms.TextBox txtApellidoSolicitante;
        private System.Windows.Forms.TextBox txtNombreSolicitante;
        private System.Windows.Forms.Label lblMailSolicitante;
        private System.Windows.Forms.Label lblApellidoSolicitante;
        private System.Windows.Forms.Label lblNombreSolicitante;
        private System.Windows.Forms.GroupBox groupListaSolicitantes;
        private System.Windows.Forms.ListBox listBoxSolicitantes;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnEditarSolicitante;
        private System.Windows.Forms.Button btnEliminarSolicitante;
        private System.Windows.Forms.GroupBox groupBoxListaDesarrolladores;
        private System.Windows.Forms.ListBox listBoxDesarrolladores;
        private System.Windows.Forms.GroupBox groupBoxDatosDesarrollador;
        private System.Windows.Forms.TextBox txtMailDesarrollador;
        private System.Windows.Forms.TextBox txtApellidoDesarrollador;
        private System.Windows.Forms.TextBox txtNombreDesarrollador;
        private System.Windows.Forms.Label lblMailDesarrollador;
        private System.Windows.Forms.Label lblApellidoDesarrollador;
        private System.Windows.Forms.Label lblNombreDesarrollador;
        private System.Windows.Forms.GroupBox groupBoxAccionesDesarrollador;
        private System.Windows.Forms.Button btnEliminarDesarrollador;
        private System.Windows.Forms.Button btnSalirDesarrollador;
        private System.Windows.Forms.Button btnGuardarDesarrollador;
        private System.Windows.Forms.Button btnEditarDesarrollador;
        private System.Windows.Forms.Button btnNuevoDesarrollador;
    }
}